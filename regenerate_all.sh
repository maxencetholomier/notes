#/bin/bash 

files=$(find . -iname "*.md")

# TODO : Faire un makefile pour automatiser le nettoyage

for f in $files ; do
    echo "Converting file $f"
    pandoc -V block-headings -V "fontfamily:arev" -V geometry:margin=0.5in -V fontsize=11pt --highlight-style zenburn --atx-headers $f -o ${f/md/pdf}
done


