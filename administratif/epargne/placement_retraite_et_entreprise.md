# Placement retraite et entreprise


## PEE

Le plan épargne entreprise est déductible d'impôts.

## PERCOL


Le PERCOL est un placement pour la retraite.

Il est déclarable de deux façons : 


1.  Le déduire du revenu imposable dans l'année.

    Dans la déclaration d'impôt il faut ajouter :

        Epargne retraite 
        Cotisation sur les nouveaux plan d'épagne retraite (PER)/déductible du revenue global 

2 . Le retirer sans déduction fiscale à la sortie. 

