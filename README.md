# Notes

## Edit

I am no longer using this repository. I was spending too much time updating and maintaining it.
I now use [this Zettelkasten repository](https://gitlab.com/maxencetholomier/zettelkasten).

I'm keeping this repository to demonstrate why using a tree structure to store knowledge is a bad idea:

- Some items could fit into multiple sections, which leads to redundant content (check the two bash.md).
- Some notes tend to be too long and not easily readable (check wifi.md).
- The temptation to standardize all notes is very strong, leading to a waste of time (check sed.md and find.md).
- The notes lack interconnections based on architecture.

## Repository

This repository contains my personal notes in French.

As I'm still a junior engineer, I will probably make a lot of modifications to this repository.
