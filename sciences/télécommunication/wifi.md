# Couche 802.11

## Couche 1 

### Antennes

#### Définition 

Une antenne est un dispositif électronique permettant de capter ou d'emmètre des ondes
électromagnétique.

#### Type d'Antennes

| Type d'Antenne    | Description                                                                         |
|-------------------|-------------------------------------------------------------------------------------|
| Resonant Antennas | Antenne conçue pour résonner à une fréquence donnée (patches antenna, loop antenan) |
| Yahi-Uda Antenne  | fait pour être utilisée sur une large plage de fréquence                            |

#### Paramètre d'une Antenne

Bande de fréquence
Polarisation
Gain et Diagramme de rayonnement

### Matériel 

#### Dongle Wi-Fi

Pour capture le traffic Wi-Fi sur les réseau autre que celui auquel l'appareil est connecté il est
nécessaire d'utiliser ne antenne supportant le mode "Monitor". Ce dernier mode permet de capturer et de visualiser l'ensemble des paquets ce que ne permet pas le mode "Managed".

Paramètre à prendre en compte sur la chipset de l'antenne :
* Amandement (a, b, c, an, ac, ...)
* Compatibilité OS (Kali, Linux, ...)


## Couche 2 

### Type et Sous-Type

| Valeur Type | Type       | Valeur Sous-Type (binaire) | Sous-Type                      |
|-------------|------------|----------------------------|--------------------------------|
| 00          | Management | 0000                       | Association Request            |
| 00          | Management | 0001                       | Association Response           |
| 00          | Management | 0010                       | Reassociation Request          |
| 00          | Management | 0011                       | Reassociation Response         |
| 00          | Management | 0100                       | Probe Request                  |
| 00          | Management | 0101                       | Probe Response                 |
| 00          | Management | 0110                       | Timing Advertisement           |
| 00          | Management | 0111                       | Reserved                       |
| 00          | Management | 1000                       | Beacon                         |
| 00          | Management | 1001                       | ATIM                           |
| 00          | Management | 1010                       | Disassociation                 |
| 00          | Management | 1011                       | Authentication                 |
| 00          | Management | 1100                       | Deauthentication               |
| 00          | Management | 1101                       | Action                         |
| 00          | Management | 1110                       | Action No Ack (NACK)           |
| 00          | Management | 1111                       | Reserved                       |
| 01          | Control    | 0000-0010                  | Reserved                       |
| 01          | Control    | 0011                       | TACK                           |
| 01          | Control    | 0100                       | Beamforming Report Poll        |
| 01          | Control    | 0101                       | VHT/HE NDP Announcement        |
| 01          | Control    | 0110                       | Control Frame Extension        |
| 01          | Control    | 0111                       | Control Wrapper                |
| 01          | Control    | 1000                       | Block Ack Request (BAR)        |
| 01          | Control    | 1001                       | Block Ack (BA)                 |
| 01          | Control    | 1010                       | PS-Poll                        |
| 01          | Control    | 1011                       | RTS                            |
| 01          | Control    | 1100                       | CTS                            |
| 01          | Control    | 1101                       | ACK                            |
| 01          | Control    | 1110                       | CF-End                         |
| 01          | Control    | 1111                       | CF-End + CF-ACK                |
| 10          | Data       | 0000                       | Data                           |
| 10          | Data       | 0001                       | Reserved                       |
| 10          | Data       | 0010                       | Reserved                       |
| 10          | Data       | 0011                       | Reserved                       |
| 10          | Data       | 0100                       | Null (no data)                 |
| 10          | Data       | 0101                       | Reserved                       |
| 10          | Data       | 0110                       | Reserved                       |
| 10          | Data       | 0111                       | Reserved                       |
| 10          | Data       | 1000                       | QoS Data                       |
| 10          | Data       | 1001                       | QoS Data + CF-ACK              |
| 10          | Data       | 1010                       | QoS Data + CF-Poll             |
| 10          | Data       | 1011                       | QoS Data + CF-ACK + CF-Poll    |
| 10          | Data       | 1100                       | QoS Null (no data)             |
| 10          | Data       | 1101                       | Reserved                       |
| 10          | Data       | 1110                       | QoS CF-Poll (no data)          |
| 10          | Data       | 1111                       | QoS CF-ACK + CF-Poll (no data) |
| 11          | Extension  | 0000                       | DMG Beacon                     |
| 11          | Extension  | 0001                       | S1G Beacon                     |
| 11          | Extension  | 0010-1111                  | Reserved                       |


## Paquet Action : Code d'action

Champs trouvé sur le net.

| Valeur du code d'action | Code d'action                   |
|-------------------------|---------------------------------|
| 0                       | Spectrum Management             |
| 1                       | QoS                             |
| 2                       | DLS                             |
| 3                       | Block ACK                       |
| 4                       | Public                          |
| 5                       | Radio Measurement               |
| 6                       | Fast BSS Transistion            |
| 7                       | High Throughput (HT)            |
| 8                       | SA Query                        |
| 9                       | Protected dual of public action |
| 10-125                  | Reserved/Unused                 |
| 126                     | Vendor specific protected       |
| 127                     | Vendor specific                 |
| 128-255                 | Error                           |

Champs trouvé sur Wireshark

| Valeur du code d'action | Code d'action         |
|-------------------------|-----------------------|
| 0                       | Add Block Add Request |
| 1                       | Add Block Add Reponse |
| 2                       |                       |
| 3                       | Block Ack             |
| 4                       |                       |
| 5                       |                       |
| 6                       | BTM Query             |
| 7                       | BTM Request           |
| 8                       | BTM Response          |
| 9                       |                       |
| 10-125                  |                       |
| 126                     |                       |
| 127                     |                       |
| 128-255                 |                       |

## Transmitter   

A supprimer ?

Type = 0

qos rtds
autentification rdts
association rdts
action rdts
action no action rdts
bss transition management rtds

Type = 1

blockack rt
blockack_req rt
vht/he Announcement rt

powersavepoll (0x001a) r


Type = 2
data rtds 
eapol key rtds
qos rtds 
qosdata rtds 


## Pacquet QoS : EAPoL Key

| Numéro du message | Information de la clef |
|-------------------|------------------------|
| 1                 | Ox008a (138)           |
| 2                 | Ox010a (266)           |
| 3                 | Ox13ca (5066)          |
| 4                 | Ox030a (788)           |

# Amandements 802.11

## Message 802.11 v

Correspond au BSS Transition Management.
Encourage fortement la box à steerer sur un AP.

## Message 802.11 k

Correspond au Neighbour report.
Informe le devices des meilleurs Accès point environnant.

# Remarque

## TCP dans le Wi-Fi

Le tcp n'est pas idéal pour le traffic Wi-FI. Ce dernier a été conçu à l'origine pour des réseaux
fillaire très congestionné.

En non-fillaire il y a beaucoup de problèmes intrinsèque avec aux Wi-Fi qui remonte au niveau 4 avec
TCP. Il est recommande d'utiliser UDP qui ne cachent pas les problème.
