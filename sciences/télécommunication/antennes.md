# Antenne

## Définition 

Une antenne est un dispositif électronique permettant de capter ou d'emmètre des ondes
électromagnétique.

## Type d'Antennes

| Type d'Antenne    | Description                                                                         |
|-------------------|-------------------------------------------------------------------------------------|
| Resonant Antennas | Antenne conçue pour résonner à une fréquence donnée (patches antenna, loop antenan) |
| Yahi-Uda Antenne  | fait pour être utilisée sur une large plage de fréquence                            |

## Paramètre d'une Antenne

Bande de fréquence
Polarisation
Gain et Diagramme de rayonnement


## Polarisation


