# HTTP

## Définition

Le protocole HTTP est un protocole de communication permettant d'échanger des données.

Il a été rendu populaire car contrairement au ftp il comprenait des en-tête permettant de décrire le type de fichiers transmis.

## Fonctionnent 

### Type de message

| Message |  Description                                                                                                             |
|---------|--------------------------------------------------------------------------------------------------------------------------|
| GET     | les données à envoyer au serveur sont écrites directement dans l’URL, les utilisateur peuvent voir la requête            |
| POST    | les paramètres écrit les paramètres URL dans la requête HTTP pour le serveur, les utilisateur ne peuvent voir la requête |

