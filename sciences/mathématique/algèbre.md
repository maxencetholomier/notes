# Algèbre

## Définition :

Branche des mathématiques portant sur la résolution des équations et l'étude 
des structures (groupes, anneaux, corps, ...).

## Historique :

| Type                 | Description                                                                                                          |  
|----------------------|----------------------------------------------------------------------------------------------------------------------|
| Algèbre classique    | Résolution des problèmes de balance avec des équations, introduction à la notion de lettre (AL-Jabr)                 |
| Géométrie Algébrique | Représentation des figure géométrique par des équations algébrique, y = f(x) (Descartes)                             |
| Algèbre moderne      | Représentions des math suivant des entités plutôt que des quantités, introduction à la notion de nombre complexe. () |
| Algèbre linéaire     | Etude des espace de dimension n à l'aide d'équation algébrique, représentation sous la forme de matrice.             |


## Programme ATS :

Résolution équation second degrés.
Résolution équation différentielles
Calcul Matriciel.


