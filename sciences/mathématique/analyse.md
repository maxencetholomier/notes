Définition : 
Branche des mathématiques portant sur les définition de limites et les calculs infinitésimaux. 

Historique :
Début de l'analyse 	Etude des nombre infini et notamment de PI.
	Approché une première fois via des polygone(Archimède). Assez compliqué à mettre en place.
	Approché une deuxième fois par des séries infinies (Madhava). Beaucoup plus pratique.
Apparition de dérivé 	Etude des différences infinitésimales. 
	A permis calculer la vitesse instantanée d'une pomme (Newton).
Apparition des séries 	Etude de l'approximation de fonction par leurs dérivée (Taylor et LaGrange). 
	Apparition des relation d'Euler 
Apparition des équation dif 	Notion d'équation différentiel. Permettant d'étudier les équations comprenant une variable et sa dérivée. 
	Par exemple la position de la balançoire dépend de position précédente et de l'accélération.
	Mise en place de théorème d'existence de solution (Cochi).
	Mise en place de théorème de description de solution via des série(Fourier).
Apparition des Topologie 	Rien compris. (Pointcarré)

 

Programme ATS :
Notion de limite.
Notion de continuité.
Notion dérivation.
Notion d'intégration.
Notion de différentielle.

