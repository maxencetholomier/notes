# Circuits intégrée

## Définition Circuit Intégré 

Un circuit intégré est un circuit électronique composé d'éléments distincts 
intégré sur le même substrat.

## Classification des circuits intégrés : 

 | Type de circuit | Reprogrammable  | Type d'exécution   | Composition                                           |
 | --------------  | ---------------- | ------------------ | ----------------------------------------------------- |
 | Processeur      | Oui              | Procédurale        | ALU CPU Séquenceur Registre de travail                |
 | DSP             | Oui              | Procédurale        | ALU CPU Séquenceur Registre de travail Accumulateur   |
 | Microcontrôleur | Oui              | Procédurale        | Processeur  Périphérique (USB GPIO ADC ...)           |
 | GPU             | Oui              | Parallèle          | CPU                                                   |
 | FPGA            | Oui              | Parallèle          | CLB Matrice Interconnexion Entrées / Sorties          |
 | ASIC            | Non              | Procédurale        | ?                                                     |
 | SoC             | Oui              | Mixte              | Processeurs FPGA Mémoires                             |


## Comparaison performance circuits intégrés : 

Pour définir quel est le composant le plus performant il faut regarder la 
finesse/largeur des traces de silicium. 

