##  Définition :

Un FPGA est un composant reprogrammable composé des éléments suivants :

* Block logique (CLB ou configurable block Logique)
* Matrice interconnexion
* Block d'entrée sortie (Input/Output block)

Et accessoirement des éléments suivants :

* Mémoire
* Processeur 
* Convertisseur analogique numérique.

## Choix du FPGA : 

Pour choisir un FPGA on regarde généralement 

* Le nombre de CLB
* Le nombre d'entrée sortie 
* Les périphérique associé (ADC et autre)

## Etape de génération du bitstream :

| Etape          | Détail                                                                                                      |
|----------------|-------------------------------------------------------------------------------------------------------------|
| Simulation     | Vérifier rapidement des partie de code VHDL car la synthèse prend pas mal de temps.                         |
| RTL analysis   | Visualiser le circuit produit par notre code VHDL.                                                          |
| Synthèse       | Vérifier s'il existe des erreur dans le code VHDL.                                                          |
| Implementation | Implémentation du code VHDL sur la plateforme hardware. Cette étape permets e vérifier les erreur de clock. |
| Bitstream      | Génération du binaire pour le FPGA cible.                                                                   |

## Questions :

Comment peut-on lié les signaux du FPGA avec les pin. 
Comment faire une machine à état en FPGA. 
Revoir le cours de FPGA. 
