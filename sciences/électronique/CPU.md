# CPU


## Architecture d'un CPU


Il faut que la mémoire et les registres de travail soit liés.


                                 .--------------------------.
                                 | Ordonnanceur             |
                                 |                          |
                                 | Placer mémoire 1 dans R1 |
                                 | Placer mémoire 2 dans R2 |
                                 | Additionner R1 et R2     |     
                                 | Mettre le résultat dans R3
                                 |                          |
                                 '--------------------------'
                                               |
                                               |
                                               |
                                               |
     .--------------------------.       .------------.         .--------------.
     | Registre de travail      |       | Processeur |         | Mémoire      |
     | R1                       |       |            |         |              |
     | R2                       |-------| +          | ------->| 1 : 00010101 |
     | R3                       |       | -          |         | 2 : 01010101 |
     '--------------------------'       | *          |         | 3 : 01010101 |
                                        '------------'         '--------------'
                                               |
                                               |
                                               |
                                               |
                                      .------------------.
                                      | Périphériques    |
                                      |                  |
                                      | Carte Wi-Fi      |
                                      | Carte Ethernet   |
                                      | Carte USB        |
                                      | Carte Audio      |
                                      | Carte graphique  |
                                      '------------------'





