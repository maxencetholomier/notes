# Allacrity

## Présentation : 

Allacrity est un émulateur de terminal créé en 2017 par Joe Wilm.

Spécificité : 
* Uitilise le GPU plutôt que le CPU afin d'augmenter sa vitesse de traitement.
* Ne comporte pas de tab ni de split. Cette tâche est laissé aux "terminals multiplexers"
	
## Raccourcis : 

| Raccourcis       |  Description                            |  
|------------------|-----------------------------------------|
| ctrl+shift+space | Mode édition                            |
| ctrl+Shift       | Selectionne le texte                    |
| ctrl+shift+C     | Colle le texte dans le clipboard system |

