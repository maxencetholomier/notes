# Git  commandes

##  Compte

```bash
# Définir le nom utilisateur
git config --global user.name "votre_pseudo"

# Associe une adresse email au git
git config --global user.email moi@email.com
```

## Création repertoire git

```bash
# Clone le projet 
git clone <URL http ou SSH >

# Définie le dossier courant comme workspace git
git init
```

## Historique

```bash
# Affiche l'ensemble des commit du projet
git log                

# Affiche l'ensemble des différences entre tous les commit successif
git log -p             

# Ne montre que les deux dernier commits
git log -2             

# Les commits sont affiché sur une ligne
git log --oneline      

# Les commit sont affiché sous forme de graphe
git log --all --graph  
```

## Status

```bash
# Différence  entre le workspace et le local repository 
git diff   
```

## Index

```bash
# Ajoute le fichier à l'index
git add <fichier>             

# Ajoute l'ensemble des fichier à l'index
git add   .                   

# Enlève un fichier de l'index et du workspace
git rm <fichier >             

# Desinxédé le nom du fichier.
git reset HEAD < fichier >    

# On annule les modification effectué sur un fichier.
git checkout -- < fichier >   
```

## Commit

```bash
git commit -m "mon_premier_commit" # Effectue le commit et le nomme msg
git commit <fichier>               # Ajoute le fichier au local repository
git commit --amend                 # On commit mais sur la même version.
```


## Branch

```bash
git log --oneline               # Permet de déterminer les différente branche
git branch <nom_branch>         # Créer une branch testing
git checkout <nom_branch>       # Permet de basculer sur une branche existante
git checkout -b iss53           # Créer la branche et y fait pointer HEAD dessus
git branch -d test              # Suppression d'une branche (si tout les modification ont été effectué)git branch -d test
git branch -D test              # Suppression d'une branche (même si les modification n'ont pas été apporté.
```

## Stash

```bash
git stash                # Ajoute les modification dans une stash (visible dans --graph)
git stash list          # Affiche les stash qui ont été crée
git stash drop stash@{1} # Supprime l'avant dernier stash
git stash apply          # Applique les dernier stash sauvegardées
```

## Dépot distant

```bash
git remote -v                                # Affiche l'ensemble des dossier cloné.
git remote show [nom-distant]                # Toutes les information sur un dépot distant.
git remote add [nomcourt] [url]              # Ajouter un dépot distant avec un nom court.
git fetch [remote-name]                      # Récupération des donnée du dépot distants, ne modifie pas la branche de travail
git push [nom-dépot] [nom-de-branche]        # On pousse nos modification sur un branche du dépot.
git remote remane [avant] [apres]            # Renome le nom court d'un dépôt.
git remote rm paul                           # Supprime le dépot local.
```

## Gitignore

Pour retirer un fichier de l'indexation il faut l'ajouter à gitignore, le supprimer puis le recréer. 
