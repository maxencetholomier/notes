# Git 

Git est un gestionnaire de version créé en 1999 par Linus Torvald.

Il embarque les fonctionnalitées suivantes : 

- Traçage des modifications via des tables de hashage
- Optimisation des données sauvegardées via des références et non des copies
- Opérations effecutés en local 

## Utilisation :
   
### Variables 

    GIT_TRACE : historique des commandes et alias utlisée par git
 
### Etats des fichiers :

    Validé   : Données stocké en sécurtié sur la base de données
    Modifié  : Fichiers validé mais pas stockés en base
    Indexé   : Fichier indiqué modifié pour faire partie du prochain instantané

### Formatage URL dépot distant

    URL HTTPS  : https://gitlab.com/maxencetholomier/dotfiles
    URL SSH    : git@gitlab.com:maxencetholomier/dotfiles.git

## Exemple d'utilisation
   
#### Commiter des modifications

Représentation des dépots :

    Dépot cloud               master
                                |
    A-B-C-D-E-F-G-H-I-J-K-L-M-O-P


    Dépot origin 

             master
              |
    A-B-C-D-E-F
               \
                1-2-3-4-5
                        |
                       dev
                        |
                       head

Indexer :

    git add .

Commiter :

    git commit -m "<nom du commit>"

Représentation des dépots :

    Dépot cloud               master
                                |
    A-B-C-D-E-F-G-H-I-J-K-L-M-O-P-


    Dépot origin 

             master
              |
    A-B-C-D-E-F
               \
                1-2-3-4-5-6
                          |
                         dev
                          |
                         head

#### Tirer les moditications du cloud

Représentation des dépots :

    Dépot cloud               master
                                |
    A-B-C-D-E-F-G-H-I-J-K-L-M-O-P-


    Dépot origin 

             master
              |
    A-B-C-D-E-F
               \
                1-2-3-4-5-6
                          |
                         dev
                          |
                         head

On se positionne sur le master :

    git checkout master

On tire sur le master :

    git pull origin master

Représentation des dépots :

    Dépot cloud               master
                                |
    A-B-C-D-E-F-G-H-I-J-K-L-M-O-P-


    Dépot origin 
                               head
                                |
                              master
                                |
    A-B-C-D-E-F-G-H-I-J-K-L-M-O-P-
               \
                1-2-3-4-5-6
                          |
                         dev

####  Fusionner les modifications avec celles du cloud

##### Merge

Représentation des dépots :

    Dépot cloud               master
                                |
    A-B-C-D-E-F-G-H-I-J-K-L-M-O-P-


    Dépot origin 

             master
              |
    A-B-C-D-E-F
               \
                1-2-3-4-5-6
                          |
                         dev
                          |
                         head

On se place sur le master :
    
    git checkout master

Sur  master on vient merger les branches :

    git merge dev

Représentation des dépots :

    Dépot cloud               master
                                |
    A-B-C-D-E-F-G-H-I-J-K-L-M-O-P


                                 head
                                  |
                                master
    Dépot origin                  |
    A-B-C-D-E-F-G-H-I-J-K-L-M-O-P-Q
               \                  |
                1-2-3-4-5-6--------
                                  |
                                 dev

##### Merge Fast-Foward ( A compléter)

Représentation des dépots :

    Dépot cloud               master
                                |
    A-B-C-D-E-F-G-H-I-J-K-L-M-O-P-


    Dépot origin 

             master
              |
    A-B-C-D-E-F
               \
                1-2-3-4-5-6
                          |
                         dev
                          |
                         head

Sur  master on vient merger les branches :

    git merge  dev

Représentation des dépots :

    Dépot cloud              master
                                |
    A-B-C-D-E-F-G-H-I-J-K-L-M-O-P-

    Dépot origin 

             head
              |
            master
              |
    A-B-C-D-E-F
               \
                1-2-3-4-5-6
                          |
                         dev

#####  Rebase

Représentation des dépots :

    Dépot cloud              master
                                |
    A-B-C-D-E-F-G-H-I-J-K-L-M-O-P-


    Dépot  origin 

                      head
                        |
                      master
                        |
    A-B-C-D-E-F-G-H-I-J-K
                         \
                           L-M-
                             |
                            dev

On se remet sur ma branche :

    git checkout dev

On récupére les modifications de la branche master :

    git rebase master

Représentation des dépots :

    Dépot cloud               master
                                |
    A-B-C-D-E-F-G-H-I-J-K-L-M-O-P-


    Dépot origin

                      head
                        |
                      master
                        |
    A-B-C-D-E-F-G-H-I-J-K-O-P-1-2
                                |
                               dev

On se reconnecte sur le master :

    git checkout master

On fait un git rebase :

    git merge dev

Représentation des dépots :


    Dépot cloud               master
                                |
    A-B-C-D-E-F-G-H-I-J-K-L-M-O-P-


    Dépot origin 

                               head
                                |
                              master
                                |
    A-B-C-D-E-F-G-H-I-J-K-O-P-1-2
                                |
                               dev
