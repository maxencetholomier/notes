# Présentation : 

Le logiciel tcpdump permet de capturer des trace pcap.

# Utilisation 

tcpdump -D    Liste l'ensemble des interfaces capturable sur le PC
tcpdump     Capture le trafic sur toutes les interfaces
tcpdump -i any
tcpdump -i <interface>    Capture le trafic sur l'interface sélectionné
tcpdump -c50    Capture 50 messages.
tcpdump icmp    Capture seulement les paquet ICMP
tcpdump -nn    Désactive la résolution DNS
tcpdump host <IP>    Capture les paquets dont l'adresse ip spécifié est destinataire ou source. 
tcpdump src   <IP>    Capture les paquets dont l'adresse IP spécifié est source.
tcpdump dst   <IP>    Capture les paquets dont l'adresse IP spécifié est source.
tcpdump  <IP>  -w fichier.txt    Capture l'ensemble des paquets dont l'adresse spécifié est source et les place dans fichier.txt

Combinaison de filtre  : 

tcpdump -i nas1 port 80  host 192.168.10.1 

tcpdump -i nas1 igmp

tcpdump -i nas1 port 80  host 192.168.10.1 and host host 192.168.10.12

tcpdump -i  eth4.3500@eth4  

Ne pas limiter la taille des paquest : 

