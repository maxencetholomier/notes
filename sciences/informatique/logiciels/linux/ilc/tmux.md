# Tmux 

## Présentation

Tmux est un logiciel en ligne de commande créé en XXXX par XXXX.

## Utilisation

### Ameçon

```bash
tmux set-hook  -t "$SESSION" 'after-split-window' 'send-keys "pipenv shell" C-m'
```


### Commande Utile

Copier une chaine de caractère

```bash
tmux setb "ma chaine de caractère"
```

Copier depuis l'entrée une chaine de cacractère

```bash
echo " ma chaine de caractère " | tmux loadb -
```


## Remarque

Il n'existe actuellement pas de raccourcis permettant de créer une nouvelle session.
Pour créé une nouvelle session il faut entrer en mode comande et taper "new".

## Exemple Création session

```bash
#!/bin/bash

# COMMANDE : tdotfiles.sh
# NAME : Tmux Dotfiles
# DESCRIPTION : Launch my tmux's Dotfiles session

SESSION="dotfiles"

WINDOW_1="dotfiles"
COMMAND_1="vim ."

WINDOW_2="notes"
COMMAND_2="cd $notes && vim ."

cd $dotfiles

tmux new-session -d -s $SESSION
tmux rename-window -t $SESSION:1 $WINDOW_1
tmux send-keys -t $SESSION:$WINDOW_1 "$COMMAND_1" C-m

tmux new-window -t $SESSION:2 -n $WINDOW_2
tmux send-keys -t $SESSION:$WINDOW_2 "$COMMAND_2" C-m

if [[ -z $TMUX ]]; then 
    tmux attach-session -t $SESSION:1
else 
    tmux switch -t $SESSION:1
fi
```
