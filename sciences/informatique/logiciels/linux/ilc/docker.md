# Dockers

## Présentation

Docker est un logiciel créer par Solomon Hykes en 2008.

## Utilisation


### Sources

L'ensemble des images docker de base sont stockées sur le [site web](https://hub.docker.com/\_ubuntu) 
de la fondation docker.


### Création d'une image 

Créer un dossier dockerfile et y ajouter l'image de base. 

```bash
mkdir projet && cd projet
touch Dockerfile 
echo "FROM debian" > Dockerfile
```

Créer l'image.

```bash
docker build  -t <nom conteneur> .
```

### Execution d'une image

Pour exécuter une image interactivement il faut ajouter l'option -it.

```bash
docker run -it <image> bash 
```

### Manipulation des images 

Supprimer une image 

```bash
docker image rm  <tab>
docker rmi <tab>
docker rmi -f $(docker images -aq) # Supprimer toutes les images
```

### Manipulation des conteneurs

Tuer un container.

```bash
docker kill <tab>
```

### Montage de volume


Pendant la phase de développement il peut-être pénible de rebuilder le conteneur a chauque
changement.

Pour éviter cela il est préférable de monter le repertoire de développement dans le conteneur. 

Ainsi chaque changement du code seront vu par le conteneur.

```bash
docker run \
-v $PWD:<repertoire dans le container> \
-it <nom du conteneur>
```


### Exemple Dockers Files


```Dockerfile
# set base image (host OS)
FROM debian:11

# set the working directory in the container
WORKDIR /code

# intall pip
RUN apt update
RUN apt install -y pip
RUN apt install -y vim

# copy the dependencies file to the working dir
COPY requirements.txt .

# install dependencmes
RUN pip install -r requirements.txt

# copy the content of the local src directory to the working dir
COPY src/ .

# Pourquoi ça ne marche pas ? 
# copy my config
COPY config/.vimrc . 
COPY config/.bashrc /home
```
### Résolution problème DNS

```bash
docker run <image> nslookup google.com
```
