# Awk 

## Présentation : 

awk est un logiciel en ligne de commande créé en 1977 par Afred Aho, Peter Weinberger et Brian 
Kernighan.

Il permet de filtrer des données structuré (csv, xml, ..) via le langage de programmation awk.

Son utilisation est déconseillé lorsque les données ne dispose pas de parttern spécifique comme 
les fichier json ou xml. Dans ce cas un langage de script comme python est bien plus pertinant.

## Utilisation :

Affiche la première colonne de la commande ps 

``` bash
ps | awk '{print $1}'
``` 


Affiche les utilisateurs de l'ordinateurs 

``` bash
awk -F ":" '{ print $1 }' /etc/passwd
``` 


Affiche les trois première colonnes des des systèmes de fichiers contenant "/dev"

``` bash
df | awk  '/\/dev/ { print $1"\t"$2"\t"$3 }'
``` 


Affiche les lignes contenant plus de 7 lettres dans le fichiers /etc/shells

``` bash
awk  'length($0) > 7 ' /etc/shells 
``` 
