# Présentation 

Iperf est un logiciel en ligne de commande créé en XXXX par XXXX.

Il permet d'effectuer des tests performance entre un deux machines.

# Utilisation 

#### Lauch a server 
    iperf3 -s -p 5001 

#### Lanch a client 
    iperf3 -c  <adresse ip > -p 5001 

### Lanch a client and exchange 30000 messages 
    iperf3 -c  <adresse ip > -p 5001  -t 30000


iperf -c <client> -R    Les données vont du serveur vers le client

# Remarque

Au niveau des tests iperfs, il faut toujours récupèrer la valeur minimale. 
C'est elle qui est affiché sur les outils comme Nperf en ligne.
