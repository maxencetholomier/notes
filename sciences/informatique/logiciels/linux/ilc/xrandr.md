# Xrandr

## Présentation

Xrandr est un logiciel permettant de contrôler les moniteurs relié à un PC utilisant un serveur
graphique X-org.

## Utilisation

Liste les moniteurs connectés au PC de tests

``` bash
xrandr
```

Allume HMDI-0 et le place à la gauche de DVID-D-0

``` bash
xrandr --output HDMI-0 --auto  --left-of DVID-D-0
```



 xrandr --output eDP-1 --auto --right-of HDMI-2
