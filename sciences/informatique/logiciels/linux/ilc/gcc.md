# Gcc

## Présentation

"The Gnu Compiler Collection" est un logiciel en ligne de commande crée par
Richard Stallman en 1987.

Il permet de compiler des fichiers source en fichiers binaires.

## Utilisation

#### Décomposition étapes compilation

```bash
# Preprocessing
gcc -E <fichier source> -o <fichier preprocesser> 

# Production code assembleur
gcc -S <fichier source>                           

# Production code objet
gcc -c <fichier source>                           
                                                  
# Compilation + Edition de lien 
gcc <fichier(s) objets> -o <programme>            

```

| Etape                      | Description                                                          |
|----------------------------|----------------------------------------------------------------------|
| Preprocessing              | Création du fichier C complet (ajout  prototype fonction de la libc) |
| Production code assembleur | Convertion en assembleur                                             |
| Production code objet      | Convertion en language machine                                       |
| Edition de lien            | Lie les codes objets entre eux                                       |



### Variables


| Variables         | Description                                                          |
|-------------------|----------------------------------------------------------------------|
| LD_LIBRAIRIE_PATH | Variable contenant les différentes librarie |

### Options principales


| Option                | Description                                                                                      |
|-----------------------|--------------------------------------------------------------------------------------------------|
| -c <fichier source>   | Compile le fichier source mais n'effectue pas le linkage                                         |
| -o <fichier>          | Fixe le nom du fichier cible créé au lieu du nom par défaut a.out.                               |
| -g                    | Ajouter la table des symboles à l'executable lors de la compilation                              |
| -Wall                 | Active la plupart des warning de compilation                                                     |
| -Wextra               | Active des warning de compilation  qui ne sont pas activé par -Wall                              |
| -Werror               | Bloque la compilation si des Warning sont présents                                               |
| -ansi                 | Désactive certaines fonctionnalités de gcc pour le rendre (presque) conforme à la norme C89      |
| std=C[99, 90, 89]     | Meme chose de précédemment avec d'autre normes                                                   |
| -pedantic             | Stipule à gcc d'appliquer une conformité totole à la norme spécifié par l'option ansi ou std est |
| -O[1, 2, 3][s]        | Niveau d'optimisation du programme                                                               |
| -y                    | Parallélisation des tâche de compilation                                                         |
| -I </path/to/header>  | Inclu le Header des fonctions pour le préprocessing                                              |
| -l </path/to/library> | Inclu la librairie pour l'édition des liens                                                      |
| -L .                  | Inclu les bibliothèque du repertoir courant                                                      |
| -fPIC                 | (Position Independant Code) La librairie dynamique peut-être positionné n'importe ou en RAM      |


### Librairie

Il existe deux type de librairie : 

 - Statique : chargées dans l'éxecutable lors de la compilation. 
 Généralement présentes dans l'embarqué. L'extention est en .a.

 - Dynamique : chargées en RAM et utilisables par par plusieurs processus en même temps.
 Généralement présente sur les système Unix. L'extension est en .so.

### Table des symboles

Après avoir ajouter l'option de debbug -g, l'on peut consulter la table des symboles via la
commande suivantes :

```bash
     nm -a <Nom executable>
```
