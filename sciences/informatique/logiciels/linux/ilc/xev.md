# Présentation : 

Xev est un logiciel en ligne de commande créé par Jim Fulton.

Il permet de visualiser les serial events des appuis bouton utilisateur.

Ces informations sont très pratiques pour remapper des commandes.

# Utilisation 

Taper xev pour lancer l'executable 

Appuyer sur j renverra : KeyPress event, serial 44.

Pour fermer xev il faut envoyer un signal de fermeture depuis l'environnement 
graphique (Gnome, Awesome, ...).
