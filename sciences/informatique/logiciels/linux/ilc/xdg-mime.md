# xdg-mime

## Présentation :

xdg-mime est un logiciel permettant de définir des logiciels par défaut pour la commande xdg-open.


## Utilisation 

### Commande de base

Afficher quel est le type xdg d'un fichier. 

```shell
xdg-mime query filetype <file>
```

Définir sxiv comme logiciel par défaut pour les png.
 
```shell
xdg-mime default sxiv.desktop image/png
```

Les logiciels par défauts sont disponible dans le dossier "/usr/share/applications".
