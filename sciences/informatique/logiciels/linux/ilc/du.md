# Présentation : 

Disk usage est un logiciel en ligne de commande créé en XXXX par XXXX.

Il permet d'évaluer la taille d'un dossier/fichier.

# Utilisation : 

    du dossier	Donne l'espace occupé par "dossier" en kilo octet.
    du -h dossier	Donner l'espace occupé par "dossier" dans le bon ordre de grandeur.

