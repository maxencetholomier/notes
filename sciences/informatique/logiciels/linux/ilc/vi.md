# Présentation

# Utilisation 

## Copier une ligne 

```vi
:w!xclip -o
```

## Ouvrir rapidement un fichier

Ouvrir vi et lister les fichiers du projet.

```bash
:.! find . 
```

Rechercher le fichier et le copier dans le clipboard système. 

<!-- TODO: Regarder pourquoi ça ne marche pas dans nvi mais dans vim oui  -->

```bash
.w !xclip -selection clipboard
```



