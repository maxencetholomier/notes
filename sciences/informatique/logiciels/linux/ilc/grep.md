# Grep 

## Présentatiom


## Utilisation 

| Option | Description              |
|--------|--------------------------|
| -o     | Only show the output     |
| -l     | Only show the file       |
| -h     | Remove filename          |
| -v     | Remove the matching line |
| -e     | Multiple Expression      |
| -E     | Extented regex           |
| -P     | Perl regex               |




