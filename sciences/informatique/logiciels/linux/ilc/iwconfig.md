# IWCONFIG

## Présentation 

Iwconfig est un logicel en ligne de commande permettant de visualiser les 
information des interfaces sans fils

Ce logiciel est maintenant obsolète. Iw est recommandé pour le remplacer.

## Utilisation 

Configurer le canal d'une interface :

    sudo iwconfig <interface> <channel>


