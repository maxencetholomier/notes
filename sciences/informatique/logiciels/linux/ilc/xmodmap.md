# Xmodmap

## Présentation


## Utilisation

### Fichier de configuration

Le fichier de configuration s'appelle ".Xmodmap"
Pour le sourcer : 

```bash
xmodmap .Xmodmap
```

### Mapper echap sur maj

```xmodmap
clear Lock
keycode 0x42 = Escape
! keycode 0x29 = Return
```

### Mapper Super sur Menu


```xmodmap
keycode 0x87 = Super_L 
```

### Souris en mode défaut

```bash
pointer = 1 2 3 4 5 6 7 8 9 10 11 12
```
