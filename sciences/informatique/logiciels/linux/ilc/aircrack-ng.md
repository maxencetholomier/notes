# Aircrack

## Présentation

Aircrak-NG est un logiciel permettant de 

- Capturer des traces Wi-Fi
- Forcer des protocole de sécurité (WEP, WPA, WPA2 ...)

## Utilisation

| Commandes                        | Descriptions
|----------------------------------|---------------------------------------------------------------------------|
| sudo airmon-ng check kill        | affiche les processus qui bloquent le passage de  l'antenne à "monitored" |
| sudo airmon-ng start <interface> | Lance la capture sous format                                              |
| airodump-ng <interface >         | lance la capture wifi                                                     |


this is a cool
