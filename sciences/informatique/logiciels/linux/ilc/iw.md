# IW 

## Présentation 

IW est logiciel en ligne de commande permettant de manipuler les interfaces
sans fil.

## Utilisation 

```bash
    iw dev                             # Liste les interfaces
    iw  <interface> set type monitor   # Passage Monitor Mode
                             managed
    iw dev <interface> info            # Supprime l'interface
    iw dev <interface> del             # Suppression de l'interface
```

```bash
    
    # Fixer la channel et la largeur de band
    sudo iw dev <interface> set channel <channel> [<largeur canal>]
```
