# Awesome

## Présentation

## Utilisation

### Raccourcis 

Gérer plusieurs écrans :  

    Mod+o      Envoie l'application sur l'autre écran
    C+Mod+j    Focus sur l'écran précédant suivant
    C+Mod+k

Gérer plusieurs tabs  :

    Shift+mod+j    Réorganisation des tabulation
    Shift+mod+k


### Configuration sans Widget

Monter et baisser le son

```lua
awful.key({}, "XF86AudioRaiseVolume", function () awful.util.spawn("amixer -D pulse sset Master 10%+", false) end),
awful.key({}, "XF86AudioLowerVolume", function () awful.util.spawn("amixer -D pulse sset Master 10%-", false) end),
awful.key({}, "XF86AudioMute", function () awful.util.spawn("amixer -D pulse sset Master toggle", false) end),
```

Augmenter et diminuer l'éclairage

```lua
awful.key({}, "XF86MonBrightnessUp", function () awful.util.spawn("brightnessctl s \"10+%\"", false) end),
awful.key({}, "XF86MonBrightnessDown", function () awful.util.spawn("brightnessctl s \"10-%\"", false) end),
```

### Element


Wibar    : barre contenant les widget en haut par défaut
Menu     : Liste déraulante lorsque l'on fait un click droit
Launcher : Bouton en haut à gaucher par défaut
