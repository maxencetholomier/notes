# Présentation : 

SSH est un client ssh en ligne de commande . 


# Utilisation 

    ssh <nom utilisateur>@<url ou ip>    Se connecte en SSH au serveur ssh du nom de domaine
    ssh-keygen
    ssh-copy-id         Copie la clef ssh de mon PC sur le serveur distant
