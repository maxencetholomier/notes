# IPROUTE2 

## Présentation 

IPROUTE2 est une suite logicielle en ligne de commande permettant de manipuler 
les interfaces réseaux.

Elle vise à remplacer nettool et est par défaut l'outil présent sur la plupart des distribution linux depuis 2017.

## Utilisation 

### Adresse 

```bash
ip a add <ip> dev <interface>   # Ajoute IP interface 
ip a del <ip> dev <interface>   # Supprimer IP interface
```

### Route  

Une route par défaut pointe vers une machine alors qu'une route standard 
pointe vers un ensemble de machine.

```bash
ip route add <ip réseau>/<mask> dev <interface>     # Ajoute route
ip route add default via <ip iad> dev <interface>   # Ajoute route par défaut
ip route del <ip réseau>/<mask>                     # Supprime route
ip route del default via <ip iad>                   # Supprime route par defaut
```


### Lien up/down   

```bash

ip link add name br0 type bridge    # Création and configuration bridge
ip link set br0 up
ip link set enp10s0 up
ip link set enp10s0 master br0

ip link add link eth0 name eth0.8 type vlan id 8  # Création interface vlannisé
ip link set <interface> [<up>|down]               # Activation 
```

### Table arp

```bash
ip neigh
```


### Bridge

```bash
bridge link
```


### VLAN

```bash
bridge vlan
```


