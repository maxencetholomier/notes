# Find

## Présentation 

Find est un logiciel en ligne de commande créé en XXXX par XXXX.
Il permet de trouver des fichiers par via la génération de nom de fichiers.


## Utilisation 

### Rechercher une expression 

Cherchons un fichier dans le répertoire /etc.

```bash
find /etc -<option> <expression>  -exec <operateur>
```

| Option | Description                                     |
|--------|-------------------------------------------------|
| -path  | recherche uniquement dans les nom de dossier    |
| -name  | recherche uniquement dans les fichiers          |
| -regex | recherche avec les regex dans le chemin complet |



| Opérateur | Description                                                                  |
|----------|------------------------------------------------------------------------------|
| \{\}     | Sortie de la commande find                                                   |
| ;        | Fin de la commande "--exec"                                                  |
| \;       | Fin de la commande "--exec" échapée pour ne pas être interpréer par le shell |
| +        | Equivalent à la précédentep  |
| \;       | Fin de la commande "--exec" échapée pour ne pas être interpréer par le shell |



