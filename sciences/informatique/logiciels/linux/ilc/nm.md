# NM 

## Présentation

nm est un outil permettant de lister les symbole présent dans un fichier objet.

## Utilisation

```bash
nm file.o
```
