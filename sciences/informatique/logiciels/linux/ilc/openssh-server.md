# Présentation : 

Openssh-server est un serveur SSH en ligne de commandes. 

Il s'exécute automatiquement au démarrage de l'ordinateur. 

# Remarque : 

etc/ssh/sshd_config    Fichier de configuration 

L'on peut configurer sur le serveur l'utilisation d'un mot de passe ou de la clef publique. 


