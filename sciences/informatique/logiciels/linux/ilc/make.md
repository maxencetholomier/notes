# Présentation 

Make est un programme en ligne de commande développé par Stuart Feldman en 1978 au sein du Bell Labs.

Il permet d'automatiser des actions répétitives comme la compilation ou l'installation de logiciels. 

# Utilisation 

## Fichier

Make interpréte des les fichiers nommée "Makefile".
Ces fichier doivent être créer dans le répertoire contenant le code C.

Le M majuscule est nécessaire pour que celui-ci soit interprété par make.  

## Instructions

### Règle 

Une règle est une instruction prenant la forme suivante : 

```makefile
<cible> : <dépendance>
<TAB> <commande>
```

Cette règle n'est exécuté que si la dépendance est plus récente que la cible. 

Dans le cas où la cible n'est pas un fichier il est nécessaire d'ajouter une ligne au fichier
makefile afin de lui préciser d'exécuter la cible à chaque appel de make.

```makefile
.PHONY : cible
```

En effet si ce un fichier dans le projet porte le même nom que la cible, 
alors il existe un risque pour que la règle associé à cette cible ne soit pas exécuté.


Par défaut lors de l'exécution d'un makefile, l'ensemble des commandes exécuté sont affiché.
Pour désactiver cette fonctionnalité il est nécessaire de mettre un @ devant le nom de la commande. 


Les tab permettent d'indiquer à make d'exécuter la commande via un  sub-shell.

NB : chaque ligne de la recette est exécuter par un sous-shell. Si une variable est définie à une 
ligne elle ne seras pas disponible la ligne suivantes.

### Boucle

Lister les fichier sources.

```makefile
CODEDIRS= . lib
CFILES=$(foreach D,$(CODEDIRS), $(wildcard $(D)/*.c))
```


## Variables 

### Variable simple

```
GCC = gcc
```

### Variables spéciale

```makefile
    $@    #  nom cible 
    $<    #  nom première dépendance
    $^    # liste dépendances
    $?    #  liste dépendance plus récente que la cible
    $*    #  nom fichier
    $(@D) #  nom du répertoire de la cible
```

## Intéraction avec le shell 

Récupérer la valeur d'une commande shell via une **initialisation simple**.

```makefile
XX := $(shell date) // date will be executed once
tt:
    @echo $(XX)
    $(shell sleep 2)
    @echo $(XX)
```

Récupérer la valeur d'une commande shell via une **initialisation étendu**.

```makefile
XX = $(shell date) // date will be executed every time you use XX
tt:
    @echo $(XX)
    $(shell sleep 2)
    @echo $(XX)
```

Echapper le dollar.

```makefile
shell_command := link=$$(find . -type l); echo $$link
```

# Exemple  Générique

## Mono-dossier

```makefile
DEBUG = yes
CC = gcc
EXEC = prog 
SRC = $(wildcard *.c)
OBJ = $(SRC:.c=.o)
ifeq ($(DEBUG),yes)
	CFLAGS=-W -Wall -ansi -pedantic -g
	LDFLAGS=
else
	CFLAGS=-W -Wall -ansi -pedantic
	LDFLAGS=
endif


all : $(EXEC)
ifeq ($(DEBUG),yes)
	@echo "Génération en mode debug"
else
	@echo "Génération en mode release"
endif

 
%.o : %.c
	$(CC) -o $@ -c $< $(LDFLAGS)

$(EXEC) : $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)


.PHONY: clean mrproper

clean:
	rm -rf *.o

mrproper: clean
	rm -rf prog
```

## Poly-dossier

# Remarque 

## Trailing white Space

Il faut bien faire attention à ne pas laisser d'espace à la fin des ligne. Les retours à
la ligne via "\" ne fonctionneront plus.


# Alternative à make  

Sconstruct.


# Génération automatique de makefile  

Autoconf, cmake. 


