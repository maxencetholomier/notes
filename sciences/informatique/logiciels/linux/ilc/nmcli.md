# Nmcli

## Présentation 

Nmcli est un logiciel en ligne de commande permettant de gérer le logiciel network manager. 

## Utilisation 

### Connexion

### Scan 

```bash
nmcli dev wifi
```

### Connection 

#### Réseaux non connu

```bash
nmcli dev wifi connect <ssid> password <password>
```

```bash
nmcli device wifi connect <ssid> password '<password>' ifname <interface> 
```

#### Réseaux déjà connu

```bash
nmcli connection up <tab>
```

### Récupérer les informations

Pas de commande disponible malheuresement. Pour ça il faut aller regarder dans les fichiers de 
configurations suivant.

 - /etc/NetworkManager/system-connections
