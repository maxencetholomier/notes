# Printf

## Présentation

## Utilisation

Afficher un caractère unicode

```bash
printf '\U0001F63C'
```

Afficher un caractère UTF8

```bash
printf '\xe2\x98\xa0'
```

Convertir un nombre hexadecimal en octal

```bash
printf "%o\n" 0x100
```
