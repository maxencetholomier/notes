# Nmcli

## Présentation 

Bluez est un logiciel en ligne de commande permettant de gérer les interfaces bluethoot.

## Utilisation 

### Installation

```bash
sudo apt update
sudo apt  install bluetooth bluez bluez-tools rfkill
sudo usermod -aG lp $USER
newgrp lp
```
### Vérification des services

Vérification activation.

```bash
systemctl is-enabled bluetooth.service 
systemctl status bluetooth.service
```

### Scan 

```bash
nmcli dev wifi
```

### Connection 

```bash
nmcli dev wifi connect <ssid> password <password>
```

### Récupérer les informations

Pas de commande disponible malheuresement. Pour ça il faut aller regarder dans les fichiers de 
configurations suivant.

    /etc/NetworkManager/system-connections
