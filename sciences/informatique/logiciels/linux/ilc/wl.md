# Wl  


## Présentation 

Wl est une logiciel en ligne de commande permettant de contrôler les puces des 
carte électronique.


## Utilisation

### Informations

Liste appareil connecté 

	 wl -i <interface>  assoclist

Paramètre radio tous les apparaeil

	 wl -i <interface> sta_info all

Paramètre appareil connectés d'un appareil

	 wl -i <interface> sta_info F4:17:B8:D8:B1:18

Version logiciel 

    wl ver

SSID

	 wl -i wl0 radar 2


### Modifications paramètres

Changer le canal d'une interface

	 wl -i <interface> channel 
	 wl -i <interface> channel <channel>


### Event simulation

Simulate a radar signal on the current DFS Channel

	 wl -i wl0 radar 2

Simulate a radar signal on the scanned DFS Channel

	 wl -i wl0 radar 3

Simulate  a high occupation of one or more channel

	 wl -i wl0 bssload_static <sta_count><chan_utili in %><available admission capacity>

Unsimulate a high occupation of one or more channel

	 wl -i wl0 bssload_static off


