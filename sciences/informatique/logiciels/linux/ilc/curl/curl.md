# Curl

## Présentation

Curl est un logiciel permettant d'interagir avec des serveurs web.

## Utilisation

Visualiser des pages web :

Renvoie le fichier html de la page www.google.com

```bash
    curl www.google.com          
```

| Commande                 | Description                              |
|--------------------------|------------------------------------------|
| curl -head               | récupére uniquement le header            |
| curl -v                  | spécifie les intéraction avec le serveur |
| curl --trace fichier.txt | enregistre le résultats dans un fichier  |


POST HTTP

    • Aller sur la  page d'aide permettant de contrôler l'API.
     https://reqres.in/

    • Puis récupérer le endpoint que l'on souhaite utiliser 

    • La méthode associé

    • Les informations associé

    • Taper la première commande pour envoyer simplement les information

    curl https://regres.in/api/users/2 -d "name=Maxence&job=engineer"

    • Taper la deuxième commande pour envoyer les information sous le format d'un JSON

    curl -X PUT https://regres.in/api/users/2 -d '{"name":Maxence,"jobs":"engineer"}'

    (Cette command à chier mais je ne connais pas les message d'erreur)

    • Si la commande réussi l'on aura une note qui informe de la bonne création de la requête :

(mettre la réponse à la précédente commande)

