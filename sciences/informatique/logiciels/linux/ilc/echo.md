# Echo

## Présentation


## Utilisation


Par défaut la commande "echo" n'interprète pas les caractères Unicode. 
Pour ce faire il est nécessaire de passer l'option "-e".

```bash
    echo -e '\xE2\x98\xA0'
``` 
