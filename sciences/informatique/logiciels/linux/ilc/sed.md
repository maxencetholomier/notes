# Sed

## Présentation


## Utilisation


### Remplacer dans un fichier

```bash 
sed -i "s/avant/après/g" monfichier.txt
```

### Remplacer dans sed si motif dans ligne

```bash 
sed '/motif/s/avant/après/"
```


### Remplacer le motif dans les ligne contenant "The".

```bash 
sed  "/motif/s/foo/bar/g" 
```


### Supprimer toutes les lignes contenant motif.

```bash 
sed  "/motif/d" monfichier.txt
```

### Execution de multiple commande sed.

```bash 
sed  -e "s/foo1/bar1/g"  -e "s/foo2/bar2/g" monfichier.txt
```


### Transforme toutes les lettres minuscules  en majuscule.

```bash 
sed  -e "s/[a-z]/\U&/g" monfichier.txt
```


### Transforme toutes les lettres majuscule en minuscules.

```bash 
sed  -e "s/[A-Z]/\L&/g" monfichier.txt
```

### Récupérer le code des touches tapées

```bash
sed -n l
```

