# Pandoc

## Présentation

Pandoc est un logiciel permettant de convertir des fichiers texte en
documents numériques.

## Utilisation

### Options importante de la ligne de commande


| Option                 | Description                                 |  
|------------------------|---------------------------------------------|
| -V block-headings      | Corrige le bug des titre 4 collé aux texte  |
| -highlight-style tango | Applique un style au block de code          |

### Problème notable

 - [Header problem](https://github.com/jgm/pandoc/issues/1658)
