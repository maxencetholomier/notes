## Présentation :

Le "Bourne Again Shell" est un interpréteur de commandes créé en 1989
par Brian Fox.

## Fichiers de configuration :

Ci-dessous les principaux fichiers de configuration de bash.

    ~/.profile           : Fichier lu par quasiement l'ensemble des shell 
    ~/.bashrc            : Fichier lu par les non-login shell
    ~/.bash_profile      : Fichier lu par les login shell 
    /etc/bash_completion : Fichier contenant l'ensemble des completion et lu par ~/.bashrc

Suivant les distributions il se peut que des dupliquas de ces fichiers se situent dans 
le dossier /etc. Linux étant un système multi-utilisateur il est nécessaire de disposer de 
fichiers de configuration s'appliquant à l'ensemble des utilisateur.

Attention !

Sans le fichier .bash\_profile le fichier .bashrc risque de ne pas être
lu. En effet ce dernier est sourcée dans .bash\_profile.

```bash
     if [ -f ~/.bashrc ]; then
        source ~/.bashrc
     fi  
```

Variable Environnent :

    EDITOR       :  L'éditeur de texte par défaut 
    TERMINAL     :  Le terminal par défaut
    TRUEBROWER   :  Le navigateur web par défaut

## Utilisation

### Raccourcis

    <C-X><C-E>  " Permet d'éditer une commande du terminal dans un buffer vim

### Selection Manpager

```bash
export BAT_PAGER='less -RF'

export MANPAGER="sh -c 'col -bx | batcat -l man -p'"

export MANPAGER='/bin/bash -c "vim -XMRn -c \"set buftype=nofile showtabline=0 ft=man ts=8 nomod nolist norelativenumber nonu noma\" -c \"normal L\" </dev/tty <(col -b)"'
```


