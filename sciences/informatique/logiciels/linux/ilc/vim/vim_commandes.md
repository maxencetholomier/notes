# Commandes Interne

## VIM

### Navigation et Recherche

### Buffer

    :%bd|e#  " ferme tous les buffers expeté le buffer courant

### Terminal

    <C-W>N " Passe le terminal en mode graphique
    A      " Quitter le mode édition

### Correction et completion

 Completion

    <C-x><C-F> " Complete le chemin sous le curseur (par exemple ./)

 Correcteur orthographe

    :set spell           " active la correction orthographique
    :set spellang=fr_ch  " configure la correction automatique en francais

### Scripting et macros

 Session

    :makesession file.vim " sauvegarde la session courante dans un fichier vim
    vim -S file.vim       " restaure la session init.vim

 Macro

    :11,142 norm! @f  " Exécute la macro f sur les lignes 11 à 142

### Remap

 Utilisation plus facile des touche numérique

    nnoremap 1 &
    nnoremap 2 é
    nnoremap 3 "
    nnoremap 4 '
    nnoremap 5 (
    nnoremap 6 -
    nnoremap 7 è
    nnoremap 8 _
    nnoremap 9 ç
    nnoremap 0 à
    nnoremap & 1
    nnoremap é 2
    nnoremap " 3
    nnoremap ' 4
    nnoremap ( 5
    nnoremap - 6
    nnoremap è 7
    nnoremap _ 8
    nnoremap ç 9
    nnoremap à 0
    vnoremap 1 &
    vnoremap 2 é
    vnoremap 3 "
    vnoremap 4 '
    vnoremap 5 (
    vnoremap 6 -
    vnoremap 7 è
    vnoremap 8 _
    vnoremap 9 ç
    vnoremap 0 à
    vnoremap & 1
    vnoremap é 2
    vnoremap " 3
    vnoremap ' 4
    vnoremap ( 5
    vnoremap - 6
    vnoremap è 7
    vnoremap _ 8
    vnoremap ç 9
    vnoremap à 0

### Divers

Comparaison fichiers :

```bash
    vimdiff file1 file2       " Ouvre vim et compare les deux fichiers
```

Comparaison fichiers :

    :vert diffsplit fichier   " Compare le fichier courant avec file
    :diffthis                 " Compare un fichier à la comparaison (il faut marquer les deux fichiers)
    :set noscrollbind         " Suppression du scrolling lié sur un des deux fichiers

Manipulation texte

    <C-a>   " Incremente le chiffre ou les chiffre sélectionnées
    g<C-a>  " Incremente le chiffre ou les chiffre sélectionnées par rapport au précédant

Affichage :

    set listchars=nbsp:.  " Afficher les différent type d'espace

Affichage variable :

    :h keycode     Affiche le nom de variable associé à chaque touche

Execution multiple commande, le caractère "bar" représente le caractère pipe :

    :badd file.txt <bar> b file.txt <bar> .!ls la <cr>
    . " Repete la derière indentation

## Plug-in

### Netrw

Chaché les fichiers masqués.

    let g:netrw_list_hide = '^\..*'        " or anything you like
    let g:netrw_hide = 1                   " hide by default

### Conquer of Completion

Pyright se base sur le premier python trouvé dans avec la commande 'which python'.

