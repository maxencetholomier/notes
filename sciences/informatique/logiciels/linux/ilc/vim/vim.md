# Présentation

VIM est un éditeur de texte sous liscence libre créé par Bram Moolenaar en 1981.

Basé sur l'éditeur Vi, il en reprend le fonctionnement
général et y ajoutes plusieurs fonctionnalitées, dont notamment :

 - La coloration syntaxique
 - Un système de fenêtrage
 - Un outil de comparaison de fichiers
 - L'utilisation de macro via le language vim script

 Cette dernière fonctionnalité lui permet d'étre facilement
personnalisable. De nombreuse extensions externes, appellé
Plug-In sont disponibles et maintenus par une communauté de
bénévoles.

## Utilisation

## Fichier de configuration

La configration de Vim s'effecute via des fichiers nommés .vimrc.

Au démmarage vim vient sourcer ces fichiers dans les répertoires suivant :

 - home folder (~/.vim)
 - sysadmin folder
 - \$VIMRUNTIME
 - sysadmin after folder

Des répertoires supplémentaire peuvent être ajoutés via la commande

    set runtimepath = <folder>

## Indexation chemin

Par défaut Vim n'indexe pas les repertoire et sous répértoire
du dossier courant.

Pour ce faire il faut ajouter la commande suivante dans vimrc

	set path += **

Pour indexer les site package python :

	set path += /usr/local/lib/python3.8/dist-packages
	set path += ~/.local/lib/python3.8/site-packages

## Indexation symbole

Vim par défaut n'indexe aucun symbole.

Pour indexer les symbole d'un projet il faut :

- Utiliser les variables include et includepath (nb: recherche regex dans le path vim)
- Utiliser Ctags
- Utiliser un plug-in, comme Conquer of Completion (nb recherche les symboles
dans path uniquement)

## Regex

Vim utilise par défaut les Basic Regular Expression.

## Copier-coller

### Presse papier systèmes

Par défaut vim n'utilise pas le presse papier sytème. Pour activer cette option
il faut installer vim-gtk et ajouter la commande suivante  dans vimrc.

    set clipboard=unnamedplus

On peut sinon utiliser la redirection avec xclip :

    :.w !xclip -selection primary

### Tabulation copier-coller

Lorsque du texte est copié/collé dans vim depuis l'extérieur, un problème de
tabulation apparait. Pour l'éviter il faut taper la commande "set paste".

## Listes

### Change list

La change list de Vim est une fonctionnalité qui permets de tracer l'ensemble
des modifications apportées au(x) texte(s) traité(s).

### Jumps List

La jump list est une fonctionnalité qui permet de tracer l'ensemble des
déplacements effectuées sur les fichiers.

### Quickfixlist

En programmation c, lorsque l'on compile et que l'on commet une erreur il est
possible de jumper directement à cette erreur en utilisant la quickfixlist.

# Remarque

## Unix philosophie

Vim suit la philosophie Unix. Il fait une chose et le fait bien.

Il est ainsi possible de se passer la plupart des plug-in.

Par exemple pour remplacer fzf l'on peut mettre nos buffer dans
/tmp et les localiser avec la commande command -v ou which.
