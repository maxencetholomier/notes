# Nmap 

## Présentation  

Nmap est un logiciel en ligne de commandes permettant de scanner les port d'un ou d'un ensemble de terminal. 

## Utilisation 

Scan de réseau  

nmap -sP 192.168.1.0/24    afficher l'ensemble des machine du réseau en envoyant un ping à l'ensemble des machine du réseau 

Scan de port   

    nmap localhost      Scan les port de son propre PC
    nmap 192.168.1.0    Scan les port de l'adresse ip 
    nmap 192.168.1.0/24    Scan les port de l'ensemble des ordinateur  du sous réseau
    nmap 192.168.1.*    Scan les port de l'ensemble des ordinateur  du sous réseau

On peut rajouter les option suivante 

    -sT    
    -sS    Stealth pour ne pas se faire repérer par le firewall
    -p 8,440     

## Remarque  

Je n'ai pas réussi à faire marcher nmap sur une adresse IP publique. 

