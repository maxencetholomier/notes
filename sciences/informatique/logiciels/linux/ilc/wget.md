# WGET

## Présentation

Wget un logiciel en ligne de commande permettant de télécharger des ressources
sur un serveur distant suivant de nombreux protocole de communication.

L'avantage majeur de wget par rapport à curl est la possibilité de télécharger
récursivement l'ensemble des ressources d'un site web.

Le désavantage de wget par rapport à curl est la prise en charge de moins de
protocole de communication.

## Utilisation

wget https://google.com     Télécharge le contenu de la page google en local

wget -0 <NOM_DU_FICHIER> https://google.com  
