# WPA Suppliquant

## Présentation

WPA suppliquant est un logiciel permettant de se connecter à un Acces Point via un protocole 
de sécurité WPA.

Ce logiciel est particulièrement utiliser sur les système embarqué car il est plus léger 
que des logiciel tel que Network Manager.

## Utilisation


Créer dans le répertoire courant un fichier wpa_supplicant.conf.

Y ajouter le texte suivant.

```bash
```

Depuis un terminal taper la commande suivante : 

```bash
wpa_supplicant -c <conf_file> -i <interface> -B
```


