# Présentation : 

ftp est un logiciel en ligne de commande permettant d'effectuer des requêtes ftp à un serveur ftp.

# Commande : 

    ftp <Url ou IP>    Permet de se connecter au serveur ftp du nom de domaines. 
    ls    Affiche les fichier de l'arborescence
    mget <fichier>    Récupère un fichier sur le client et le place sur le serveur
    put <fichier>    Récupère un fichier sur le serveur et le place dans le dossier courant
    rename <fichier>    Renomme un fichier de l'arborescence
    delete <fichier>    Supprime un fichier de l'arborescence 
    bye     Retourne au terminal 

