# SCP

## Présentation

Scp est un logiciel en ligne de commande permettant de copier/coller des fichiers
depuis un serveur ssh distant.

## Utilisation


    scp <fichier> <user>@<ip>:<path to file>

