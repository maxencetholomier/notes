# Arduino-Cli

## Présentation

La fondation Arduino a créer en 2018 permettant de contrôler les carte
Arduino sans utiliser l'IDE graphique.

## Utilisation

La documentation est disponible au lien suivant : 

- [Installation](https://arduino.github.io/arduino-cli/0.29/installation/)
- [Getting Started](https://arduino.github.io/arduino-cli/0.29/getting-started/)

**Installation**

```bash

# Creation du fichier de configuration 
# dans (~/.arduino15/arduino-cli.yaml)
arduino-cli config init 

# Création du Sketch
mkdir travail/personnel/code_exemples/cpp/arduino/MyFirstsketch
arduino-cli sketch new MyFirstSketch

# Connecter l'arduino au PC
arduino-cli core update-index
arduino-cli board list

# Installation des driver pour le coeur de la board
arduino-cli core install arduino:avr
arduino-cli core list


# Compilation du Sketch 
arduino-cli compile --fqbn arduino:avr:mkr1000 MyFirstSketch

```

**Ajout de driver propre à certain librairie**

Pour ajouter un packet il est nécessaire d'ajouter 
les ligne suivantes au fichier de configuration : 

```yalm

board_manager:
  additional_urls:
    - https://arduino.esp8266.com/stable/package_esp8266com_index.json
    - file:///absolute/path/to/your/package_nrf52832_index.json

```

```yalm
arduino-cli core update-index
```

**Compilation**


```bash
arduino-cli compile --fqbn arduino:avr:uno
```

**Ajout de librairie**

```bash
arduino-cli lib search debouncer
arduino-cli lib install FTDebouncer
```

**Completion**


