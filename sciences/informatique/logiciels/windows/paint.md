# Paint 

## Présentation 

Paint est un logiciel en interface graphique créé en 1985 par Microsoft.

## Astuce 

Copier une partie de l'image contenant la couleur. 
La coller sur pain.

Sélectionner la pipette et cliquer sur l'image collée. 
Cliquer sur modifier les couleurs. 
Le code couleur de l'image apparait alors.
