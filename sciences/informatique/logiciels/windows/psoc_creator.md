# Psoc Creator

## Présentation 

PSoC creator est l'IDE de développement de Cypress.
Il permet de configurer indifféremment le processeur et le FPGA du SOC.

Trois fenêtre sont importante dans l'éditeur SOC, à savoir : 

*le Top Design qui permet de manipuler les signaux des FPGA.
*le Design Wide Ressource qui permet d'assigner les signaux avec les ports physiques.
* l'éditeur de fichier C qui permet de programmer le processeur.

Contrairement à ce que l'on pourrait voir chez les concurrents (Xilinx, Altera
, ..), l'on manipule la logique via des schéma block et des fonction C associé
à ces block.

Cela est très pratique pour les personne n'ayant aucune notion en VHDL.

## Avis personnel 

Je n'aime pas cet éditeur. Les raccourcis ne sont pas classique et l'IDE est
affreusement moche.

Cependant leur banque d'exemple est très appréciable et permets à des personnes
travaillant dans les laboratoire de prototyper très rapidement.

J'apprécie également est le fait qu'un PDF s'ouvre directement lorsque l'on
ouvre l'un des exemple.

L'on a également l'onglet verticale documentation qui permet d'avoir "Acces"
rapidement à la TRM et à la documentation de chaque blockfonctions.

L'onglet verticale "Result" est également particulièrement intéressant car il
permet de voir les le temps mis à chacune des étapes du Bitstream.

