# Emulateur de terminal

## Définition

Un "Emulateur de terminal" est un composant logiciel simulant les terminaux 
de connexion comme le VT100.

## Paramètres 

### Capacité terminal

Un terminal est doté de capacité visualisable via la commande 'infocpm'. 
Ces capacité dépendent du terminal et également de la commande TERM qui permet de l'ajuster.

 - TERM=dump, pas de manipulation de curseur (= pas de vim) 
 - TERM=vt100, pas de couleur
 - TERM=256_color, couleur limité à 256

 
### Couleur

| Plan ASCII | Aucune couleur                                                      |
|------------|---------------------------------------------------------------------|
| 16 colors  | Couleurs limité à 16 nuances                                        |
| 256 colors | Couleurs limité  à 256 nuances                                      |
| True color | Aucune limitation de couleur Possibilité de visualisé des images 4K |

Les ordinateur sont actuellement assez puissance pour supporter du True Color, 
il n'y a aucune raison de rester sur le 256 colors.

Il peut y avoir des conflit si le terminal est en 256 Color et si le programme 
utilise du True Colors (comme par exemple GruvBox dans Vim).

Le logiciel en ligne de comamnde "color256" imprime l'ensemble des couleurs 
disponible sur l'émulateur de terminal.

### Contrôle du terminal

Le terminal peut-être contrôlé avec la librairie curse.

Les action possible sont : 
#TODO : lister les action

 - Netoyage de l'écran
