## Wireshark 

## Présetation 

Wireshark est un logiciel créé en XXXX par XXXX.
Il permet de capturer et visualiser les packet échangé sur les réseaux 802 (Ethernet, Wi-Fi).

## Utilisation

### Filtre

#### Opérateur logique  

| Description |   Operateur                          |
|-------------|--------------------------------------|
| ET          |   <filtre 1>  &&  <filtre 2>         |
| Ou          |   <filtre 1>              <filtre 2> |

#### Filtres Wi-Fi

ATTENTION les valeur des Type et Sous-Type doivent être renseigné en héxadécimal/décimal et non en 
bianire. 

| Description   | Filter                                         |
|---------------|------------------------------------------------|
| Bssid         | wlan.bssid==<bssid>                            |
| Type          | wlan.fc.type == 0x00<valeur type>              |
| Sous-Type     | wlan.fc.type_subtype == 0x00<valeur sous-type> |
| Eapol         | eapol                                          |
| Action code   | wlan.fixed.action_code == 0x00<Action code>    |
| Capability ax | wlan.ext_tag.number == 36                      |

#### Filtres  Ethernet  

| Description | Filter      |
|-------------|-------------|
| Tcp port    | tcp port 80 |

#### Filtres Divers 

| Description        | Filter                    |
|--------------------|---------------------------|
| La trames contient | frame contains "<chaine>" |

### Rechercher un expression dans les trames

Taper Ctrl+F.
Passer "Filtre d'affichage" à "Expression régulière"
Taper l'expression à rechercher.

### Analyser le réseaux  

Wireless-> Traffics Wlan 


### Personaliser la capture 

#### Ajouter des règle de  couleurs 
Aller dans View -> Colorize Packet List.
Ajouter une règle avec un filtre.
Aller dans View -> Colorize the packet list
