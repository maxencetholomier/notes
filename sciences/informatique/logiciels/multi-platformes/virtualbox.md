# Virtual Box 

## Présentation :

VirtualBox est un logiciel de virtualisation développé par Oracle et sortie en 2007.

## Utilisation : 

### Raccourcis :
La touche host est par défaut la touche contrôle droite. 

### Personnalisation

#### Barre des tâches :

Pour modifier la position de la barre d'outils il faut aller dans :
* Interface Configuration -> Interface Utilisateur -> Afficher en haut de l'écran. 

#### Intégration souris :

L'intégration de la souris, lorsqu'elle est activé, permet de capturer l'ensemble 
des appuis boutons.
Il est ainsi possible d'appuyer sur la touche Windows sans sortir de la machine 
virtuelle. 

#### Gestion mutiple moniteur :

Si l'on souhaite changer d'écran il faut aller dans :

* la barre d'outil de la machine virtuelle 
* puis Ecran->Ecran Virtuel 1-> Utiliser l'écran virtuelle 1/2.

#### Addition invité :

(To Do : ajouter la procédure permettant d'installer les addition invité sur Ubuntu)

#### Configuration Réseau : 

Attention 

* Lorsque l'on copie un machine virtuel sur Virtual Box, l'adresse Mac est elle 
aussi copié-collé.  Si l'on souhaire utiliser les deux machines sur le même 
réseau il faut aller changer manuellement l'adresse d'une des deux machines.

