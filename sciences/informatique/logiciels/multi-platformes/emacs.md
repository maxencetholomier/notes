# Emacs

Emacs est un logiciel crée dans les années 1970 par Richard Stallman.

## Commands

### Open serveur and client

    emacs --fg-daemon &
    emacsclient -c -a 'emacs'

### Open in terminal

    emacs -nw

## Raccourcis

     M+x       Commande emacs
     C-H C-F   Documentation sur Emacs
     C-H b     Keybinding
     C-H k     Shortcut Keybinding
     C-H h     Recherche documentation fonction
     C-G       Quitter

Org mode
    
    C-C C-C      Cocher une case
    C-C C-S      Plannifier une tâche
    C-C C-W      Déplace via un mini-buffer le header + le sous texte
    C-C C-T      Change l'état d'une tâche courantes
    M-UP M-Down  Change le header uniquement de place


Buffer Spéciaux (agenda, buffer, dired)


    C-Z: Switch entre le mode normal et le mode evil
    n:   se déplace vers le bas
    p:   se déplace vers le haut
    q:   quitter le buffer


Dired

    C   : Copie le fichier courant
    d   : Markd pour la suppression
    u   : Unmark pour la supression
    x   : execute la supression
    D   : Supprime le fichier 
    +   : Créé un repertoire
    g   : rafraichi le buffer dired
    ^   : marque un fichier
    %m  : Utilise une expression regulière pour marquer les fichiers
    t   : Inverse le marquage


## Fonction

