# Trace 32

## Présentation

Trace 32 est un logiciel de debugge graphique créée en XXXX par XXXX.

Il permet de piloter les sonde JTAG Lauterbatch.

Ce logiciel est compliqué à prendre en main mais est très modulaire et 
totalement scriptable.

## Utilisation

### Commandes

    list <fonction>      : Ouvre une fenêtre affiche la fonction afficher
    data.dump <adresse>  : Visualise la mémoire à l'adresse précisée.

### Fenêtre principale

    per:  : permet de visualiser l'état des périphérique

