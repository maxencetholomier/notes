# Eclipse

## Présentation :

Eclipse est IDE graphique créée en XXXX par XXXX.

Il permet de gérer des projets dans de nombreux languages de programmation.

## Raccourcis :

    ctrl+alt+a      :  outil sélection colonne.
    ctrl+o          :  recherche (fonction, méthode, task).
    ctrl+shift+alt  :  formatage du fichier suivant les options de formatage en vigueur.
    ctrl+i          :  formatage des espace et des tabs suivant les options de formatage en vigueur.
    ctrl+shift+t    :  liste les type et java package  ouvert.
    ctrl+shift+/    :  mets en commentaire le texte sélectionné.
    ctrl+alt+flèche :  permet surligner l'ensemble des mots similaire

## Configuration :

    Windows->Perspective->Reset perspective : reset l'agencement des différentes fenêtre des vues Eclipse.
    Windows->Editor->Toggle editor          : clone la fenêtre du fichier qui est visualisé par Eclipse 
    Double clic sur une fonction/option     : on peut directement renommer la fonction dans tous les fichier


## Problèmes courants : 

Quand il y a un problème sous Eclipse le mieux est de supprimer le projet depuis 
eclipse sans supprimer les source, de supprimer les dossiers et fichiers de 
fonctionnement et de réimporter le projet (import/Existing Code as Makefile Projet). 
Ce genre de problème arrive lorsque l'on a fait une fausse manip avec l'éditeur. 

J'ai eu des problèmes lorsque j'utilisais mes propres makefile  avec l'indexeur.
En effet je n'avais pas de problèmes à la compilation mais toutes les variables 
des .h était indexé comme une erreur dans les .c. (unresolved inclusion but compiled)

Je n'arrive pas à effectuer des recherche sur l'ensemble des bibliothèques 
car les recherche ne sont effectuées que dans le workspace contenant mon code 
source.
Pour effectuer les recherches sur l'ensemble des fichiers je rajoute simplement 
l'ensemble des header à mon projet principal :

* aller dans New -> folder -> Advanced -> Link to alternate location 
* rentrer le chemin vers les header dans mingwin.

