# Résolution de problemes embarqués

## Ecriture registre impossible

Quand on arrive pas à écrire dans un registre avec un driver en développement il
est nécessaire de vérifier avec le débogueur si l'on peut écrire dans les registres
(périphérique dans T32 ou avec un pointeur), si la valeur à écrire dans le
registre est correcte et si l'on écrit bien dans la bonne adresse.

## Echange de trames impossible

Quand on arrive pas à échanger des trames avec un autre périphérique il
est nécessaire de vérifier l'architecture matérielle avec deux Arduino par exemple.
Lorsque l'on est sûr de son coup on peut ensuite passer à la version logicielle.

## Différence entre execution complete et pas à pas

Si le comportement d'un driver n'est pas le même en run ou en pas à pas c'est
qu'il y a un problème de temporisation.

## Erreur lors la modification du flag  d'optimisation du code

Lorsque les paramètres de compilation d'un driver ont été optimisé et que l'on
tombe dans un if-else d'erreur ce n'est pas forcément celui qui a déclencher
l'erreur.

Il suffit que les deux if-else d'erreur ai le même message d'erreur et le
debugger se dirige vers le if else qu'il a déjà passé.

## Fonctionnement d'une partie des pins et pas de l'autre

Lorsqu'une partie des pin est configurées mais pas l'autres il faut vérifier
s'il n'y a pas eu un décalage :

* Au niveau des registre définie dans la structure (un registre Unused non declaré)
* Au niveau de la boucle d'écriture dans les registres.

## Fonctionnalité introuvable dans le register mapping

Il arrive sur les plateforme disposant d'un logiciel de configuration que des
registres soient caché.

Pour les trouver il est nécessaire de configurer la plateforme puis d'enregistrer
le fichier d'init.
Refaire la même manœuvre en reconfigurant les paramètres que l'on ne trouve pas
dans le TRM et enregistrer de nouveau le fichier d'init.
Comparer ensuite ces deux fichier pour trouver les fichiers à écrire.


## Une des écriture registre ne marche pas

Parfois il arrive que le registre mapping du constructeur ne soit pas correct.
Il faut dans ce cas générer notre configuration avec le logiciel fournis par le
constructeur et vérifier les registre que l'on souhaite configurer
sont à la bonne valeur ou non.


## Mauvaises trames transmisses

Si l'on ne reçoit pas les bonnes valeurs des trames des bus de communication
il faut vérifier que les données écrites dans la FIFO sont correcte.

Pour cela il faut mettre un Breakpoint juste avant l'écriture.

## Problème registres haut bas GPIO

Il faut toujours mettre un oscilloscope pour vérifier la tension que l'on
applique sur le 0GPIO. J'avais un problème de branchement et la tension était
toujours à 0. Je pensais que la non modifications des registres était software
a cause de cela.

## Problème Acknowledment slave I2C

Il faut bien faire attention a l'ordre dans lequel on écrit dans les registres.
J'avais mal placé le passage à 0 du hold bit le bit de stop n'était pas envoyé
par l'esclave.

## Problème temporisation message

Le temps entre deux trames n'etait absolument pas respecté par ma requête
d'écriture i2c sauf quand la tempo était supérieur à 2ms,
la durée constante que j'avais entre deux message.
Il semblait y avoir un addition entre la tempo et les 2ms.

Au final mon programme était bouclé dans une boucle de sécurité.
