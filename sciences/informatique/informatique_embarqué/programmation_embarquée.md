# Programmation embarquée

## Type de programmation embarqué

### Programmation Bare-Metal

La programmation en bare-metal un type de programmation très bas-niveau

Les périphériques du CPU/SoC sont programmé via les registres constructeurs.

Ces registres ne doivent pas être confondu avec les registres de travail du processeurs.

Les avantages sont les suivants :

| Types d'architecture                       |  Avantage                                    |
|--------------------------------------------|----------------------------------------------|
| Architecture simple (STM32)                | Simplicité de programmation                  |
| Architecture complexe (Ultrascale + MPSoC) | Maitrise complète du logiciel et testabilité |
|                                            |                                              |

### Programmation via BSP

### Programmation noyau

La programmation noyau est un type de programmation bas-niveau.

Les périphériques du CPU/SoC sont  programmé  via un noyau temps réel.

Les avantages sont les suivants :

| Types d'architecture                       |  Avantage                    |
|--------------------------------------------|------------------------------|
| Architecture simple (STM32)                | Meilleure gestion des tâches |
| Architecture complexe (Ultrascale + MPSoC) | Meilleure gestion des tâches |

Les désaventage sont les suivants :

| Types d'architecture                       |  Avantage   |
|--------------------------------------------|-------------|
| Architecture simple (STM32)                | Testabilité |
| Architecture complexe (Ultrascale + MPSoC) | Testabilité |

## Remarque

Pourquoi écrire ces propres makefiles ?

On maitrise le processus, on connait l'ensemble des flag utilisé.
On peut ajouter soit même des extension comme C++test.
On peut générer une multitudes de binaires (elf, so, a).
On peut refaire la table des symbole pour les tests unitaire.
	Passer une variable de local à privé ou de privé à local.
		(pour l'atteindre si elle est dans un fichier privé ou pour la refaire
		 si elle est dans un fichier non privé).


Trouver pourquoi on utilise une bibliothèque .a et pas .so en embarqué ?

Les bibliothèques dynamiques sont des bibliothèques permettant à de nombreux
processus d'utiliser des ressource commune. En embarqué les ressources ne sont
pas partagé pour des raisons de performance. (To do : à vérifier)




## Exemple Structure Driver

###  Fonctions minimales

open	Déclare une structure registre et associe cette structure avec les registre hardware du périphérique. Renvoie un handler vers cette structure.
init	Remplit la structure registre avec les paramètre définie par les utilisateurs.


### Structures

La structure reg est une structure contenant les registres du périphérique de l'architecture cible ainsi que les saut entre registre ( appelé unused_x).

La structure device est la structure contenant la structure reg et un booléen qui indique l'ouverture et la fermeture de ma structure.

La structure p_device est le pointeur de structure vers device. C'est ce handler que l'on vient fournir à l'utilisateur lorsqu'il utilise la fonction open.

Lorsque qu'il y a plusieurs modules il faut être capable de renvoyer plusieurs handler.  On utilise pour cela un tableau all_p_device contenant les p_device.

Fonctions :

La fonction open permets de vérifier que l'instance du périphérique n'a pas été ouvert et renvoie un handler vers l'instance ouverte.

La fonction init permets de linker les paramètre utilisateur avec les registre physique d'une architecture.


### Fichiers


| Fichiers   | Description                                                                                                  |  
|------------|--------------------------------------------------------------------------------------------------------------|
| .priv.h    | Contient les structure (device + structure données) les registres, les constantes (instance, offset, masque) |
| Dans le .h | on rajoute la structure utilisateur et les constante de j.                                                   |
| Dans le .c | on rajoute le code.                                                                                          |


**Exemple**


Dans stm32_can_priv.h

    ##define STM32_CAN_REGISTER_1_OFFSET  0u
    ##define STM32_CAN_REGISTER_2_OFFSET  0u

    ##define STM32_CAN_REGISTER_1_MASK  1u  << STM_32_REGISTER_1_OFFSET
    ##define STM32_CAN_REGISTER_2_MASK  1u  << STM_32_REGISTER_1_OFFSET
    ....

    typedef stm32_can_reg;
    struct stm32_can_reg {
        registre_1;
        registre_2;
        unused_1;
    };

    typedef struct stm32_can_device;
    struct stm32_can_device {
        stm32_can_reg register;
        bool open;
    };


Dans stm32_can.h

    ##define STM32_CAN_PARAMETER_1_ON    1u
    ##define STM32_CAN_PARAMETER_1_OFF   0u
    ##define STM32_CAN_PARAMETER_2_ON    1u
    ##define STM32_CAN_PARAMETER_2_OFF   0u

    typedef stm32_can_user_parameters;
    struct stm32_user_parameters {
        parameter_1;
        parameter_2;
        ...
    };


Dans stm32_can.c

    stm32_can_device* p_stm32_can_device ;

    p_stm32_can_device open (void) {
        if ( p_stm32_can_device.open == 1)
        {
            return NULL;
        }
        else
        {
            return p_stm32_can_device;
        }
    }


    p_stm32_can_device init (p_stm32_can_device p_device, stm32_can_user_parameters* p_user_parameters) {
        if ( p_user_parameters == STM32_CAN_PARAMETER_1_ON)
        {
            p_device.parameter_1 | = STM32_CAN_REGISTER_1_MASK;
        }
        else
        {
            p_device.parameter_1 &~= STM32_CAN_REGISTER_1_MASK;
        }
        if ( p_user_parameters == STM32_CAN_PARAMETER_2_ON)
        {
            p_device.parameter_2 | = STM32_CAN_REGISTER_2_MASK;
        }
        else
        {
            p_device.parameter_2 &~= STM32_CAN_REGISTER_2_MASK;
        }

    }

Dans main.c

    stm32_can_user_paramerters ;


## Documentation à récupérer  pour tout nouveau projet embarqué

    Technical Reference manual
    Register mapping.
    Routage des pin de sortie.
    Installation du SDK.
    Programme d'exemple.
