# SoC

## Définition 

Un System on Chip est un système complet intégré dans une puce électronique.

Les SoC contiennent généralement les élements suivant :

* CPU,
* GPU,
* DSP,
* SDRAM,
* Contrôleurs USB
* FPGA pour les plus puissant

## Exemple de Soc

|  Soc                     | Description                                                                                                    |
|--------------------------|----------------------------------------------------------------------------------------------------------------|
| Broadcom BCM2835         | SoC contenant l'ensemble des périphérique d'un micro-ordinateur                                                |
| XCVU13P-1FHGB2104EES9919 | SoC contenant l'ensemble des périphérique utilisé pour les application lourde (traitement d'image par exemple) |

