## Xilinx Ultrascale +

## Présentation 

Xilinx Ultrascale+ est une architecture de SoC utilisé pour les application complexes.

## Remarques

### Entrée Sorties 

#### Technologies

| Type d'entrée   | Description                                   |
|-----------------|-----------------------------------------------|
| CMOS            | Peuvent prendre l'état 1 ou l'état 2          |
| Bascule Schmitt | Aucune idée et Xilinx ne savent pas non plus. |

#### Débit

##### Low Rate / High rate 

Cette configuration a été apporté pour ne pas créer de famine lorsque
l'on utilise plusieurs périphériques de débit différents sur une même 
entrée/sortie.

A partir de quel moment peut-on dire que le périphérique est Low ou High ?
Je n'ai pour l'instant pas la réponse.

