# Arduino

## Présentation

Arduino est une famille de carte de developpement à processeur Atmel conçu par
Massimo Banzi.
Elle a pour vocation à être programmer de façon indépendante du matériel en C++
via la bibliothèque  Arduino.

## Remarques

De nombreux modèle d'Arduino, comme l'Arduino Uno, ne disposent pas de liaison
de debbug. Il faut ainsi corriger le programme via des printf.
