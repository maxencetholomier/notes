# Définition

L'encodage est une opération permettant de transformer
un caractère ou un ensemble de caractères en une suite de bits.

Cette opération vise à rendre les fichiers humains compréhensibles par les
machines et inversement. 

# Normes
  
## Historique

Dans les années 60, chaque terminal utilisait son propre système d'encodage. 

Les ordinateurs [https://fr.wikipedia.org/w/index.php?title=IBM_1130_et_1800&tableofcontents=1](IBM 1130 et IBM 1800) 
étaient par exemple livré avec leurs table de conversion. 

Pour les programmer il fallait dans un premier temps écrire le programme en Cobalt puis 
le convertir en binaire à l'aide des abacles.

Ce programme binaire était alors inscrit sur une [[https://fr.wikipedia.org/wiki/Carte_perfor%C3%A9e][carte perforée]] puis inséré dans la machine.

Ce système n'était pas idéal car lors du changement de matériel, l'ensemble des programmes devaient 
être re-encodé.

## ASCII

    Acronyme : "American Standard Code For Information Inter‐change" 

    L'ASCII est une norme de codage qui a été créée en 1975 afin de palier au précédant 
    problème.  

    Les caractères de la langue anglo-saxonnes ont été associées à un Code en base 10.
    Par exemple la lettre A est associé au code '65'. 
    
    Ce code est alors converti en base 2 afin d'être interprétable par la machine. 
    

    | Caractère | Code base 10 | Code base 2 |
    |-----------+--------------+-------------|
    | A         |           65 |     1000001 |
    | B         |           66 |     1000010 |


    NB : Pour plus d'information consulter sur la table ascii, allez regarder la page man ascii.
    
    Cette norme bien qu'ayant simplifié l'interopérabilité, dispose cependant de deux inconvéniants majeurs : 
     
    - L'encodage des caractères autre que l'anglais n'est pas possible
    - Les caratères spéciaux tel que les emojii ne sont pas représentable. 

    Ce faisant chaque pays à alors commencé à utiliser leur propre système de codage pour représenter 
    leurs languages ...

## Unicode
   
    Acronyme : Universal Character Encoding

    L'Unicode est une norme permettant d'associer à chaque graphène (plus petite entitée orale d'un language) un 
    'code point'.

    Elle a été créée en 1991 et est maintenant largement adopté à travers le monde.

    Contrairement à l'ASCII, l'Unicode ne spécifient pas comment les 'code point' sont convertis sour format binaire.
    
    Les caractère sont simplement 

## UTF-8

    L'UTF8 est une norme de codage en informatique permettant de convertir les 'code point' des graphèmes Unicode
    sous format binaire. 

    Il est totalement compatible avec la norme ACSII et est aujourd'hui majoritairement adoptée.

    Pour plus d'information consulter la page man utf-8.
    
    Cette norme est cependant décriée car elle avantage majoritairement la langue anglaise, où chaque caractère est représenté
    par un unique octet comparé à deux octects comparée à deux pour la majorité des autres langues.


## Bilan


    ASCII  -------------> Binaire

    UNICODE----UTF8-----> Binaire
               UTF16

    
# Spécificitées des système d'exploitation 
  
## CRLF

    Le retour à la ligne sur les fichiers texte n'est pas signalé de la même manière
    pour les différent OS.

	Linux : \n
	Windows : \r\n
	MacOs : \r

    Le Carriage Return est symbolisé par le caractère \n
    Le new line est symbolisé par le symbole \r.

    Historiquement l'on distingue c'est deux caractère car à l'époque des machine
    à écrire il était courant de revenir au début de la ligne, par exemple pour
    mettre en gras du texte en retapant dessus.

    Mettre à la ligne et passer à la ligne suivante était donc deux notions différentes.
