# Paradigme 

## Définition

Un paradigme de programmation correspond à la méthode par laquelle un problème
est abordé en informatique.

## Type de paradigme

(To do : paufiner les description et ajouter des exemple)

| Type                      | Description                                                                           |
|---------------------------|---------------------------------------------------------------------------------------|
| Programmation impérative  | Le paradigme originel et le plus courant                                              |
| Programmation structurée  | Visant à structurer les programmes impératifs pour en supprimer les instructions goto |
| Programmation procédurale | À comparer à la programmation fonctionnelle                                           |

| Type                                | Description                                                                                             |
|-------------------------------------|---------------------------------------------------------------------------------------------------------|
| Programmation orientée objet        | Consistant en la définition et l’assemblage de briques logicielles appelées objets (comme en Smalltalk) |
| Programmation orientée prototype    | Qui simplifie et rend plus flexible la programmation orientée objet                                     |
| Programmation orientée classe       | À comparer à la Programmation orientée prototype (dans le contexte de la programmation orientée objet) |
| Programmation orientée composant    | (comme en OLE)

| Type                          | Description                                                                                            |
|-------------------------------|--------------------------------------------------------------------------------------------------------|
| Programmation déclarative,    | Consistant à déclarer les données du problème, puis à demander au programme de le résoudre             |
| Programmation descriptive     | À l'expressivité réduite, qui permet de décrire des structures de données (par exemple, HTML ou LaTeX) |
| Programmation fonctionnelle   | Avec laquelle un programme est une fonction au sens mathématique du terme                              |
| Programmation logique         | Consistant à exprimer les problèmes et les algorithmes sous forme de prédicats (comme en Prolog)       |
| Programmation par contraintes | À comparer à la programmation logique

