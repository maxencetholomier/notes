# Expressions Regulières 

## Définition

## Histoire 

Différents type de regex BRE ERG PCRE

Sed et vim utilise par défaut la norme BRE.

## Fonctionnalitées

### PCRL

#### Lookahead

Cette fonctionnalité permet de récupérer le motif cherché s'il est adjacent ou non à un autre 
motif . 

| Expression Perl | Nom                |
|-----------------|--------------------|
| motif(?!motif)  | negative lookahead |
| motif(?=motif)  | postive lookahead  |

```bash
 echo "foo foofoo" | grep -Po '(?<!foo)foo'
```

#### LookBehind

Cette fonctionnalité permet de récupérer le reste de la ligne précédant ou succédant le motif
cherché.

NB : Uniquement supporté par les expressions régulières PERL.

| Expression Perl | Nom                 |
|-----------------|---------------------|
| (?<=motif)      | positive lookbehind |
| (?<!motif)      | negative lookbehind |


Exemple lookbehind

```bash
\# Recherche après le motif
echo "Hello World my friends" | grep -P "(?<=my).*"

\# Ne fonctionne pas 
echo "Hello World my friends" | grep -P "(?<!my).*"
```

## Méthode de tests


 - Utiliser le [site](https://regexr.com/) suivant 
 - Utiliser echo et grep 
 - Utiliser vim avec l'option incsearch
