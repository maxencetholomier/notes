# URI 

## Définition 

Une URI, Unified Ressource Identifier, est une notation permettant d'identifier une 
ressource.

Cette ressource peut-être un fichier ou une partie d'un fichier, un livre un site web ...

Une URI peut être de la forme URL ou URN, voir RFC 3968.

# URL

Une URL, Unified Ressource Location, permet d'identifier un élement par rapport à sa localisation.

Elle prend souvent la forme suivante :

    <protocole>:[//<sous domaine>.<domaine de niveau premier>][[:<port>]/<path to ressource>[? <parametres>][# <anchor>]]

Le "sous domaine" et le "domaine de niveau premier" sont appellé "nom de domaine".
Le nom de domaine et le port est appellé l'autorité.

Les paramètres sont des données traité sur le serveur.

L'anchor  sont des données traité par le client Web.

Exemple classique :

    http://www.exemple.com:80/chemin/vers/monfichier.html?clé1=valeur1&clé2=valeur2#QuelquePartDansLeDocument

Exemple classique sans Autorité

    mail:myfriend@google.com


# URN

Une URL permet d'identifier un élément indépendament de sa localisation. 
Ce concept a été avancé pour remédier aux liens cassées du net.

Voir la RFC 2141 pour en connaître la forme.
