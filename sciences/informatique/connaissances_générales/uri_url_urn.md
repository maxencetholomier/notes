# URI 

Une URI, Unified Ressource Identifier, est une notation permettant d'identifier une 
ressource sur internet.

Cette ressource peut-être un fichier ou une partie d'un fichier. 

Une URI peut être de la forme URL ou URN.

Pour plus d'information voir RFC 3968.

# URL

Une URL, Unified Ressource Location, permet d'identifier un élement par rapport à la localisation de cet élement.

Exemple d'URL :

TODO : cela méritrait de faire  un schéma

```
    <protocole>://<sous domaine>.<domaine de niveau premier>:<port>/<path to ressource>?<parametres>#<anchor>

    http://www.exemple.com:80/chemin/vers/monfichier.html?clé1=valeur1&clé2=valeur2#QuelquePartDansLeDocument
```

Le "sous domaine" et le "domaine de niveau premier" sont appellé "nom de domaine".

Le nom de domaine et le port est appellé l'autorité.

Les paramètres sont des données traité par le serveur Web.

L'anchor sont des données traité par le client Web.

Pour plus d'information voir RFC 1738.

# URN

Une URL permet d'identifier un élément via un identifiant. 

Elle peut-être comparé au numéro ISBN pour les livre.

Ce concept a été avancé pour remédier aux liens cassées du net car l'emplacement avit chant car l'emplacement avit changé

Pour plus d'information voir RFC 2141.
