# Traduction 

## Définition 

La traduction est une opération mathématique permettant de transformer du code source en 
language machine. 

## Outils de traduction

## Compilateur

Un compilateur est un outil tranformant le code sources en language machine avant 
l'execution du programme.

    Avantage      : performance
    Désavantages  : portabilité, instruction de compilation pouvant être complexes

La compilation dépend du type du processeur cible (ARM, x86, PowerPC, …) mais également de l'OS cible.
Si l'on souhaite changer d'architecture il faut recompiler le code.

#### Etape de compilation 

| Etape              | Description                                                                                      |
|--------------------|--------------------------------------------------------------------------------------------------|
| Analyse lexicale   | Le compilateur va découper le code en différent lexème. i                                        |
| Analyse syntaxique | Le compilateur va répartir l'ensemble des lexème dans une arborescence                           |
| Contextualisation  | Le compilateur va créer la table des symbole (correspondance adresse hardware et nom variable).i |
| Synthèse           | Le code va générer le code machine.

##### Edition de lien : 

C'est dans le script d'édition de lien (linker script) que l'on vient définir la taille de la pile (stack) , la taille du tas (heaps) et l'adresse des section d'assembleur. 

Paramètre de compilation :

La compilation d'un programme dépend :

	• Du type de binaire voulu (elf, so, a)
	• De l'architecture cible
	• De l'optimisation nécessaire

### Interpréteur : 

Un interpréteur est un outil tranformant le code sources durant l'execution du 
programme.

Le code est transformé en langage machine au fur et à mesure de sa lecture par l'interpréteur.

    Avantages    : simplicité,  portabilité.
    Désavantages : temps d'exection, nécéssité de l'interpréteur pour l'éxecution du code

## Cas Particulier 

### Langage semi-interprété :

Le langage est traduit en un langage intermédiaire, appelé bytes code, qui est traduit en langage 
machine à l'exécution du programme.

