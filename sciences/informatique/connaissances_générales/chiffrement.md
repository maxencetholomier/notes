# Chiffrement

## Définition

Le chiffrement est un procédé de cryptographie grâce auquel on souhaite rendre la compréhension d'un document impossible à toute personne qui n'a pas la clé de (dé)chiffrement. 

## Type de chiffrement 

###  Chiffrement sysmétrique

TODO : faire un schéma

Dans ce procédé, une seule clef permets de chiffrer et de déchiffrer le message.
Lorsque la copie de la clefs existante à été transmisse à une tierce partie **en physique**, la communication est sécurisé. 

Inconvéniant : échange de clefs en physique.

### Chiffrement asymétrique

TODO : faire un schéma

Dans ce procédé est utilisé une clé publique pour le chiffrement et une clé privée pour le déchiffrement.

Une communication half-duplex sécurisé peut-être mise en place avec un clef de chiffrement publique et 
une clef de déchiffrement privé.

Inconvéniant : Communication half-duplex.


### Combinaison des deux méthodes précédante

La combinaison des deux précédant procédé permets un échange sécurisé sur internet.

Le client envoie sa clef de chiffrement sysmétrique privée au serveur Web via un message chiffré avec la clef de chiffrement publique. 
Le serveur déchiffre le message reçut avec la clef de chiffrement privé et peut alors communiquer avec le client avec la clef de chiffrement symétrique.

