# Java 

# Définition

Java est un langage de de programmation interprété créé en 1995 par James Gosling 
et Patrick Naughton. 

Son développement résulte de la volonté de replacer la programmation en C++ 
jugée trop contraignante. En effet le C++ requiert de la part du programmeur 
une gestion de la mémoire contrairement à Java 
disposant d'un ramasse-miettes.

Ce langage est principalement utilisé pour la réalisation de logiciels de 
bureautique et d'application Android.

# Documentation

# Utilisation 

| Sigle   | Nom                        | Definition                                                        |
|---------|----------------------------|-------------------------------------------------------------------|
| JVM     | Java Virtual Machine       | Interpréteur Java est contenu dans le JRE                         |
| JRE     | Java Runtime Environnement | Environnement d'execution permettant de  lire du bytecode         |
| JDK     | Java Developpement Kit     | Module Java permettant compiler des programme en bytecode         |
| Java SE | Java Standard Edition      | Coeur du language Java                                            |
| Java EE | Java Enterprise            | Surcouche java permettant de faire des services Web, des IHM, ... |
