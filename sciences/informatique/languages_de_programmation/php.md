# PHP

## Définition 

PHP est un langage de programmation back-end (côté serveur donc) permettant 
d'automatiser des tâches comme la création des page web dynamique.

## Documentation

## Exemple 

Une page web dynamique est une page web qui est créée à la demande.
Elle contient un ensemble d'élément pouvant être ammené à changer au cours 
du temps.
Le fil d'actualité Facebook en est un bon exemple de page web dynamique car 
les article qu'il contient changent au cours du temps. 


