# Xpath

## Présentation

Xpath est un language de requêtes permettant de localiser un élément d'un fichier
XML ou HTML.

Il est très utilisée dans le domaine WEB pour l'automatisation des tests.

## Documentation


## Utilisation

### Composition Page HTML

Une page HTML est composée de balises structurées hiérarchiquement depuis la racine.

Un balise est composée :

 - de tags, permettant de repérer la balise dans la page HTML
 - d'attributs donnant à la balise des propriétées particulières


Une balise peut disposer :

 - d'une balise mère

 - d'une ou plusieurs  balise(s) fille(s)


Exemple de code HTML structuré de deux balises.

``` html
    < tag1 attr1="value1"  attr2="attr2name" >
        < tag2 attr1="value1"  attr2="attr2name" >
          Wikipédia
    </tag 2>
```

### Requêtes

#### Requête générique

Le contenu d'un attribut est requêtable de la manière suivante :

```xpath
    //*tags[@attr='value"]
```

Le \* signifie dans toute la page. Il n'est pas nécessaire de le mettre. 

Similairement plusieurs attributs peuvent être requêtables :

```xpath
    //*tag[@attr1='value1" and @attr2='value2']
    //*tag[(@attr1='value1" and @attr2='value2') or @attr3='value3']
```

Les attributs des balises filles peuvent être requêtées de la manière suivante :

```xpath
    //*tags[@attr='value"]/*/tags[@attr='value"]
    //*tags[@attr='value"]//tags[@attr='value"]
```

Les attribut de la balise mère peuvent être requêtées de la manière suivante :


```xpath
    //*tags[@attr='value"]/../tags[@attr='value"]
```

#### Requêtes à multiple résultats

Les résultats d'un requête disposant de plusieurs résultats peuvent être requêtée de la
manière suivante :


```xpath
    //*tags[@attr='value"][1]
    //*tags[@attr='value"][2]
```

#### Requêtes multiples


```xpath
    //a [.// span[@id='link']   ]
```


#### Requêtes avec expressions régulières

```xpath
//*[contains(@id, "ma_chaine")]
```
