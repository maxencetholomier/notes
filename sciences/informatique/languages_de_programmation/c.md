# C

## Définition

Le C est un langague de programmation créé en 1972 par Dennis
Ritchie et Ken Thompson.

Il ajoute une couche d'abstraction au langage assembleur et permet de s'affranchir de
la cible.

Le langage C ne fournit pas de bibliothèque ou fonctions permettant d'interagir
avec l'OS.  Il faut pour cela utiliser la bibliothèque standard du C qui a été
normalisé dans les années 80 par l'ANSI.
dkdkdkd

## Documentation

Les header de la gnu libc ne contienent pas de descriptions.

Pour se renseigner sur une fonction il faut aller sur le terminal puis taper une 
des commandes suivantes.

```bash
man libc
man <bibliothèque>
man <fonction>
```

Certaines commande linux portent le même nom que certaines fonctions C. Il faut regarder
les pages du manuel pour trouver la commande correspondante.

```bash
man 2 bind
```

## Utilisation


