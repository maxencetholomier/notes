# Bash

## Définition

Bash est le langage de script par défaut de la plupart des environnement Unix.

Il est moins rapide que d'autres interpréteurs mais sa syntaxe est extrêmement
condensé, le rendant idéal pour automatiser des tâches répétitives.

Si un script bash fait plus de 500 lignes il est recommandé d'utiliser un autre language de
programmation. Bash n'a pas vocation a être relu par un grand nombre de personnnes.

## Documentation

La documentation est disponible au lien suivant.

https://www.gnu.org/software/bash/manual/

## Utilisation

### Option d'éxecution

Ces options sont à éviter.

Arrêter le script si une commande renvoie une erreur :

    set -x

Arreter le script si une variable n'est pas définie :

    set -u

### Expension

#### Expension d'accolades \n


Création d'une suite de nombres

``` bash
echo {1..100}
```

Création d'une suite de lettre

``` bash
echo {a..z}
```

#### Expension du shell

Il est préférable d'utiliser les "$()"que des accents graves car il n'y a pas besoin d'échapper les guillemets. 

```bash 
echo "it is $(date -d "2000-11-22 09:10:15") "
```


```bash 
echo "it is `(date -d \"2000-11-22 09:10:15\")`"
```

### Virgules

#### Double virgules vs simple virgules

Les simples parenthèse interprète l'ensemble  des caractère. Il n'est pas possible d'utiliser
des simples parenthèse à l'intérieur.

Les doubles parenthèse n'interprète aucun caractère à l'intérieur excepté

    '\'
    '`'
    '$'

### Redirection entrées-sorties

    0 : entrée standard
    1 : sortie standard
    2 : sortie erreur


#### Opérateur de redirection

| Opérateur | Action                                                                             |
|-----------|------------------------------------------------------------------------------------|
| 1>        | redirection sortie standard                                                        |
| >         | similaire à la commande précédantes                                                |
| >>        | similaire à la commande précédante sans réécriture du fichier                      |
| 2>        | redirection sortie erreur                                                          |
| 2>&1      | redirection sortie erreur dans la sortie standard                                  |
| <         | redirection entrée                                                                 |
| \|        | redirige la sortie standard de gauche sur l'entrée de droite                       |
| \|&       | redirige la sortie standard et la sortie d'erreur de gauche sur l'entrée de droite |


Redirection du flux standard

``` bash
    $ cat > fichier.txt
```

Redirection du flux standard tant que le mot FIN n'apparait pas

```bash
    $ cat <<FIN > fichier
```

Redirection de la sortie d'erreur

```bash
    $ cat toto 2> erreur.log
```


Redirection du shell dans un fichier

```bash
    $ exec > fichier.txt
    $ date
    $ exit
    $ cat fichier.txt
```


Rediriger la sortie dans une variablet et dans la sortie standard

``` bash
    exec 3>&1                                   ## On créer un descripteur de fichier que l'on vient pointer sur la sortie standard
    value=$(command parametre 2>&1 1>&3)        ## On créer un sous shell et on redirige la sortie standard et la sortie standard vers le shell principale
    exec 3>&-
```

        .---------.      .---------.              .---------.
        | clavier |------|  bash   |------------->|  écran  |
        |         |      |         |       ^      |         |
        |         |------|         |-------|----->|         |
        '---------'      '---------'       |      '---------'
                              |            |
                              --------------
                                          ^
                                          |
                                          |
                                          |
                                          |
        .-----------.         .---------. |
        | parametre |---------| command |---------- value
        |           |         |         |    ^
        |           |         |         |----'
        '-----------'         '---------'

### Récupérer les paramètres d'entrée d'un script


```bash
    while test $# -ne 0; do
       case $1 in
          -e)
          -x)
       esac
       shift
    odne
```

### Echaper un caractère

Lorsque l'on utilise des "" il est nécessaire d'échapper certain caractère
tel que :

    * \\$
    * \n

### Conditions



| condition       | description                               |
|-----------------|-------------------------------------------|
| -eq 9           | équivalent à 9                            |
| -f fichier      | existance  fichier et si fichier standard |
| cmd1 && cmd2    |  cmd2 executé si cmd1 réussi              |
| cmd1  \|\| cmd2 |  cmd2 executé si cmd1 échou               |
| -n string       | true si string non v ide                  |
| -z string       | true si string vide                       |


Pour les tests il est important de bien respecter les espaces entre les [

Différence \[ \] et \[\[ \]\]

```
     [ ]  #  Utilisation des opérateur = + !
    [[ ]] #  Utilisation des opérateur == += !=
```

Condition avec expression régulière

```
    variable="tata"
    if [[ "$variable" ==  *"toto"* ]] && echo "true"
```


Condition existance fichiers bash (attention)

```
    if [[ -a *.sh ]] # si fichier *.sh exist, le * n'est pas interprété
    if [ -a *.sh ]   # Si fichier un fichier en .sh existe
```

Différence if et tests

```
    if [ -f fichier ] # cette notation
    test -f  fichier  # est équivalente à celle-ci
```

Comparaison arithmétique bash

```
    (( $var != 0 ))
    (( $var <  0 ))
```


Vérifier si une variable est à été attribuée

```bash
[ -z ${var+x} ]
```

L'expension renvoie une chaine vide si var n'as pas été attri

### Fonctions importantes

#### Read

```bash
echo "enter your name and first name"

read name            # Stocke le résultat sous la forme d'une seul variables
read name firstname  # ou sous deux variables séparer par des espace 
read -a names        # Stocke les résultats sous la forme de tableau

read -p "enter your name and first name :  " name fistname


echo "enter your password :"
read -p password

echo "type something unless 5 s "
read -t 5


```


## Bonne pratique

Afin d'éviter que des variables ne soinent pas interpretée il recommandé d'utiliser des parenthèses.


```bash
FILENAME=(a b c d)
echo $FILENAME
echo "$FILENAME"
```


