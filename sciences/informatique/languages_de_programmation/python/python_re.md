# Python RE 

## Utilisation 

### Exemple générique

```bash
import re

text_to_search ="Hello world my friend"

pattern = re.compile("w.*")

matches = pattern.finditer(text_to_search)

for match in matches :
    print(match)
```

### Méthodes 

```python
pattern.match(pattern, string, flag)                 # Match au début 
pattern.search(pattern, string, flag)                # Match n'importe où
pattern.finditer(pattern, string, flag)              # Matches + extra information
pattern.findall(pattern, string, flag)               # Matches
pattern.sub(r'<new string>', <string to search>)     #
```

### Objet de retour

```python
# Récupère l'objet "match"
regex = pattern.match(pattern, string, flag) 

# Récupère le premier match
regex.group(0) 

# Récupère le premier ou dernier match.
regex.start()
regex.end()
```
