# Python Scapy

## Utilisation 

## Objet

 - fig : objet comprenant un enêtre complète d'affichage,  composé de plusieurs plt
 - plt : objet contenant plusieurs axes

### Type de courbes


| Nom     | Definition      |
|---------|-----------------|
| plot    | courbe continue |
| scatter | champs de point |
| bar     | barre           |

### Chaîne de formattage

Les châines de formattages sont pratiques mais à éviter car elle rendent le code difficilement 
lisible.

 - [Lien mathplotlib formattage courbe](https://python-course.eu/numerical-programming/formatting-plot-in-matplotlib.php)

Il est préférable d'utiliser les options par défaut de plt.plot.

### Légende couleurs courbes

 - [Lien matplotlib couleur](https://matplotlib.org/stable/gallery/color/named_colors.html)



