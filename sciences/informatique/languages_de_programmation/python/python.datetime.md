# Python Datetime

## Documentation

 - [Documentation Datetime](https://docs.python.org/fr/3/library/datetime.html)

## Utilisation

### Objet principaux

 - time (heure, minute, seconde)
 - date (année, mois, jour)
 - datetime (année, mois, jours, heure, minutes, seconde)

### Récupérer le datetime courant

```python
import datetime

datetime.datetime.today()
```

### Définir un objet datetime

```python
import datetime
birthday = datetime.date(1995, 8, 19)

print(birthday.year)
print(birthday.mounth)
print(birthday.day)
```

### Récupérer une date non formatté

```python
import datetime
moon_landing = "7/20/1980"

moon_landing_datetime = datetime.datetime.strptime(moon_landing, "%m/%d/%Y")

print(moon_landing_datetime)
```

### Formatter une date

```python
birthday = datetime.date(1995, 8, 19)

message = "I'm born on {:%A, %B %d, %Y}."

print(message.format(birthday))
```

### Convertir un timestamp en datetime

```python
from datetime import datetime

# Carreful !
# The timestamp must be a float
timestamp = float("808826400")


# Extract the complete date
date = datetime.fromtimestamp(timestamp)

print(date)
```
