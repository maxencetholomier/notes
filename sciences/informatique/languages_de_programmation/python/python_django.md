# Python Django

## Documentation

Ci-dessous les lien vers la documentation et les tutoriels de la librairie Django.

 - [Documentation](https://docs.djangoproject.com/en/4.1/)
 - [Tutoriel Officiel](https://docs.djangoproject.com/en/4.1/)
 - [Tutoriel Corey Schaffer](https://www.youtube.com/watch?v=rHux0gMZ3Eg)

## Utilisation

**Mise en place**

**Création du projet**

```python
mkdir
pipenv install django
pipenv shell
```

Créer l'application portant le nom du dossier courant

```python
django-admin startproject django_project  .
```


Lancer le server.

```python
python3 manage.py runserver <port number>
```

Si aucun port n'est selectionné, l'application utilisera le port 8000.

L'on peut maintenant accéder à l'interface du site web en tapant l'URL suivante
dans le moteur de recherche.

 - http://localhost:8000/

Le lancement via django-admin ne fonctionne pas encore car l'application
n'est pas encore référencé dans "mon_repertoire/setting.py"

```python
django-admin runserver django_project # ERREUR
```


**Création d'une application**

```python
python manage.py startapp blog
```

Dans le nouveau dossier de l'application, ouvrir le fichier view.

Ajouter la fonciton suivante

```python
from django.http import HttpResponse

def home(request):
    return HttpResponse('<h1>Blog Home <h1>')
```

*Ajout de redirections*

Créer le fichier url

Y ajouter la redirection suivantes.

```python
from django.urls import path
from . import views

urlpatterns = [
    path("", views.home, name="blog-home" ),
]
```

Puis ajouter dans le fichier url du projet le code suivant.

```python
from django.urls import path, include


urlpatterns = [
    path("blog/", include("blog.url")),
]
```

L'on peut maintenant accéder à l'application en tapant l'URL suivante
dans le moteur de recherche.

    http://localhost:8000/blog

L'on remarque que l'url précédante ne marche plus sachant qu'il n'y a plus 
de route par défaut.


#### Template

Créer un fichier html.

```bash
mkdir -p blog/template/blog/home.html
```

Y mettre le code suivant.

```html

```

Récupérer le nom de la fonction principale dans blog/app. 

L'insérer dans la liste APP_INSTALL du fichier setting.py du projet.

```python
INSTALLED_APPS = [
    "blog.BlogConfig",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
]
```

Aller dans view et y mettre le code suivant

####



### Dossier et fichier

#### Projet Django

#### Application Django

| Dossier   | Description                                                  |
|-----------|--------------------------------------------------------------|
| migration | Génération des tables de données                             |
| admin.py  | Définie l'ésthétisme de l'application admin de l'application |
| app.py    | Configuration de l'application                               |
| module.py | Défi nition des classe de l'application                      |
| tests.py  | Tests unitaire                                               |
| view.py   | Request handler                                              |
| url.py    | Fichier liant fonctions créer avec les applications          |


#### Setting.py

Dans setting.py de l'application principale est présent les fichier la liste INSTALL_APP.

| Paramètre     | Description                                                       |
|---------------|-------------------------------------------------------------------|
| admin         | Interface admin pour gérer les données                            |
| auth          | Interface pour authentifier les utilisateur                       |
| contenttype   |                                                                   |
| session       | Héritage qui  permettait de gérer les données utilisateur         |
| messages      | Interface utilisé pour gérer les notifications pour l'utilisateur |
| stacitcsfiles | Utiliser pour gérer les fichier statique (images,audio, ...)      |

Les nouvelles applications qu sont créer doivent être ajoutée à cette liste.

