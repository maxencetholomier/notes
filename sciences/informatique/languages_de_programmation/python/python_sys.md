# Python SYS

## Utilisation

### Récupérer les argument de la cli

```python
import sys

if len(sys.argv) > 0:
    print(sys.argv[0])
```

```python3
import sys

# Basic fonction
sys.version
sys.copyright
```
