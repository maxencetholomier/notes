# Python Selenium

## Utilisation

### Installation

```bash
# Intall pip #
apt update
apt install -y pip
apt install -y curl
apt install -y wget
apt-get install -yqq unzip


# Install Google Chrome #
wget -q -O - https://dl-ssl.google.com/linux/\
linux_signing_key.pub | apt-key add -
sh -c 'echo "deb [arch=amd64] http://dl.google.com\
/linux/chrome/deb/ stable main" >> /etc/apt/sources\
.list.d/google-chrome.list'

apt-get -y update
apt-get install -y google-chrome-stable


# Install Chromedriver #
wget -O /tmp/chromedriver.zip http://chromedriver\
.storage.googleapis.com/`curl -sS chromedriver.storage\
.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip

unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/
```

### Convention

 - driver  : objet webdriver
 - element : element pointed by the webdriver

### Instancier un objet WebDriver

#### Chromium

Information sur la ligne de commande Chromium : 

 - [Utilisation Chromium en ligne de commande](https://sites.google.com/a/chromium.org/dev/developers/how-tos/run-chromium-with-flags)
 - [Option de la ligne de commande Chromium](https://peter.sh/experiments/chromium-command-line-switches/)

```python
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

CHROMEDRIVER_PATH = "/usr/local/bin/chromedriver"

chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--window-size=%s" % WINDOW_SIZE)
chrome_options.add_argument('--no-sandbox')

driver = webdriver.Chrome(executable_path=CHROMEDRIVER_PATH,
options=chrome_options)
```

### Fonctions principale

#### Naviguer

```python
dirver.current_url   # Affiche l'url courante
driver.get("<URL>")  # Go to previous Page
driver.back()        # Go back 
```

#### Trouver un élement

Soit le code HTML suivant.

```html
<input type="text" name="passwd" id="passwd-id" />
```

Quatre méthodes de recherche sont disponible pour identifier cet élément dans une page HTML.

```python
element = driver.find_element_by_id("passwd-id")
element = driver.find_element_by_name("passwd")
element = driver.find_element_by_xpath("//input[@id='passwd-id']")
element = driver.find_element_by_css_selector("input#passwd-id")
```

#### Intéragir avec la souris

##### Sans import supplémentaire

```python
# ... creation of driver and element

element.click()
```

##### Avec ActionChains

```python
from selenium.webdriver import ActionChains

# ... creation of driver and element(s)

action = ActionChains(<driver>)

# Click
action.click(<element>)
action.click_and_hold(<element>)
action.release()
action.double_click(<element>)
action.context_click(<element>) # Right click

# Move mouse
action.move_to_element(<element>).perform()
action.drag_and_drop(<element to drag>, <element to drop>)
```

#### Récupérer une chaîne de caractères

#### Position de l'element défini

```python
string_to_get  = driver.find_element_by_xpath(
"<xpath>")
```

#### Position de l'element non défini

##### Via les XPath

```python
string_to_get  = driver.find_element_by_xpath(
'//*[contains(@id, "ma_chaine")]')
```

Cette méthode ne fonctionne qu'à partir de XPATH 3.0.

##### Via le code source de la page

```python
html_page = driver.find_element_by_xpath("/html").get_attribute(
'outerHTML')
```

## Problème de version

Si la version de chromium et de chromium_driver ne sont pas compatibles il faut
ajouter une option.

```python
chromeOptions.add_argument("--remote-debugging-port=9222")
```

