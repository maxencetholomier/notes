# Python

## Définition

Python est un langage de programmation interprété créé en 1991 par Guido van Rossum.
Son nom est une référence à un sketch des Monty Python.

Ce langage est très versatile et facile d'utilisation.

Python n'a pas pour vocation de remplacer l'interpréteur de commande, ce n'est pas
un Shell et n'interagi donc pas directement avec le noyau.

L'utilisateur utilisera toujours un shell pour naviguer dans l'environnement linux.
Par exemple l'ensemble des outils permettant la compilation linux sont uniquement
disponible sur les shell.

## Documentation

### Via l'interpréteur


```python
help('os')
```
Taper la commande suivante pour avoir des information sur la fonction path de la bibliothèque OS.

```python
help('os.path')
```

### Via  Internet


## Utilisation

### Localisation bibliothèque

L'emplacement d'une bibliothèque est indiqué à la fin de la command help("<package>")

#### Site-package

Site-package est le repertoire standard de python. Il est disponible sous
 **"~/.local/lib/python<version de python>/site-packages"**.

Il contient l'ensemble des paquet installé par la commande "pip".

#### Dist-package

Dist-package est le repertoire standard de debian pour python. Il est disponible sous les repertoires
suivants : 

 - "/usr/lib/python<version de python>/dist-packages"
 - "/usr/local/lib/python<version de python>/dist-packages"


Elle contient l'ensemble des paquets installé par la commande "apt"

### Fonctions et Instructions 

#### Instruction Lamda 

L'instruction  lamdba permet de définir une fonction  dans les endoits où il est normalement 
impossible de placer l'instruction "def". 

Cette instruction est très souvent utilisé avec la méthode "sort".

```python
point2D=[(1,2), (15,1), (5,-1), (10,4)]

point2D.sort(key=lambda x: x[0]) # Tri suivant le premier élément
point2D.sort(key=lambda x: x[1]) # Tri suivant le deuxième élément

print(point2D)
```

####  Fonction Map

La fonction maps permet de modifier les élements d'une liste par l'intermédiaire d'une fonction.

```python
old_list = [1,2,3,4,5]

def func(x):
    return x*x

# Without map
new_list = []
for x in old_list : 
    new_list.append(func(x))
print(new_list)

# With map
print(list(map(func,old_list)))

```

Elle peut également être utilisé avec des liste de truples.

```python
temps = [("Berlin", 20), ("Cairo", 36), ("Buenos Aires", 19)]

c_to_f = lambda data : (data[0], (8/5)*data[1] + 32)

list(map(c_to_f, temps))
```

#### Fonction Filter

Cette fonction permet de filtrer les élements d'un liste suivant un critère.

Filtrer les élements aussi d'une valeur.

```python
old_list = [1,2,3,4,5]

list(filter(lambda x: x > 3, old_list))
```

Filtrer les élement vide

```python
old_list = ["Tokyo", "" , "Paris", "Rome", "" , "New-York"]

list(filter(None, old_list))
```

### Affichage

#### Afficher un object generator

Certains objet generator ne peuvent être affiché avec print. 

Pour les afficher il est possible de les convertir en liste puis d'afficher cette liste.

```python
import os
generator_object = os.walk(os.getcwd())

print(generator_object)          # On ne voit pas le contenu
print(list(generator_object))    # On voit le contenu
```

#### Afficher un itérable avec retour à la ligne

```bash
import os

print(dir(os))

print(*dir(os), sep='\n')
```

## Environnment virtuel

#### Concept

Un environnement virtuel est un outil facilitant la gestion des dépendances pour les projets python.

Il permet notamment :

- La séparation entre les paquets systèmes et les paquets spécifiques au projet
- Un meilleur suivi de la version des paquets utilisés
- De s'assurer de la version (2 ou 3) de python utilisée

A lire sur le sujet :

 - [Article de Towards Data Science](https://towardsdatascience.com/virtual-environments-104c62d48c54) 

NB : Docker peut remplacer cette fonctionnalité.

#### Venv

Venv permet la gestion des paquets mais ne permet pas de spécifier la version de python sur
laquelle travailler. 

La version de python utilisée par le venv est celle utilisée pour lors de se création.

##### Mise en place

Créer et activer l'environnement virtuel.

```bash
mkdir mon_projet && cd mon_projet
python3 -m venv venv/
source venv/bin/active
```

Désactiver l'environnement virtuel

```bash
deactivate
```

##### Copier l'environnement

La méthode la plus efficace de gérer l'environnement est de créer un fichier et d'y placer
les dépendances.

Ce fichier sera placer à la racine de "mon_projet".

```bash
pip freeze > requirement.txt
```

Pour le partager la personne doit recréer un environnement virtuel puis y placer le
fichier "requirement.txt".

```bash
# Install the requirement
pip install -r requirements.txt
```

##### Debbug

Le plus simple lorsqu'un problème apparait est de supprimer puis recréer

#### Pipenv

##### Mise en place 

```bash
mkdir mon_projet 
cd mon_projet
pipenv install   # Création de l'environnement virtuel
pipenv shell     # Connection à l'environement virtuel
pipenv install django
pipenv --env     # affiche la location de l'environnement virtuel
```

#### Copier l'environment

