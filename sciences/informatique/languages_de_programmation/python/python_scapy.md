# Python Scapy

## Utilisation

### Fonction de base

```python

from scapy.all import *

packets = rdpcap("exemple.pcap")

packets[0].show()                     # Display all layers
print(raw(packets[0])                 # Display all layers as hexadecimal format
print(packets[0].type)                # Display Packet field "type"
print(packets[0].getlayer(RadioTap)   # Display Packet layers "radiotap"
```

### Capture

```bash
from scapy import.all import *

packets = sniff(filter="icmp", iface=[wlan0, wlan1])
```

### Layers

### Identifier les Field et les Layers

Afficher une description des layers.

```bash
> packets[22].show()

###[ RadioTap ]###
  version   = 0
  pad       = 0
  len       = 18
  present   = Flags+Rate+Channel+dBm_AntSignal+dBm_AntNoise+Lock_Quality
  Flags     = FCS
  Rate      = 6.0 Mbps
  ChannelFrequency= 5180
  ChannelFlags= OFDM+5GHz
  dBm_AntSignal= -49 dBm
  dBm_AntNoise= -98 dBm
  Lock_Quality= 100
  notdecoded= ''

```

Y copier les fields.

```bash
>packets[22].pad
0

>packets[22].haslayer(RadioTap)
True
```


#### Layers and Fields

Type :

 - ByteField,  8 bits
 - ShortField, 16 bits

#### Ajout Layer EAPOL

Contrairement à Wireshark, l'ensemble des protocoles ne sont pas interprétés.

Ainsi si l'on essaye d'afficher une trame EAPOL du protocole 802.11, l'on obtient le résultats
suivant.

```bash
####[ EAPOL ]###
                 version   = 802.1X-2001
                 type      = EAPOL-Key
                 len       = 123
####[ Raw ]###
                 load      = '\x02\x01\n\x00\x00\x00\x00\x00\x00 ... '
```

Pour interpréter les champs qui ne sont pas compris il faut ajouter une nouvelle classe dans le
fichier /usr/local/lib/python3.8/dist-packages/scapy/layers/eap.py.

```bash
from scapy.layers.eap import *

class EAPOL_KEY(EAPOL):
    name = "EAPoL-Key"

    fields_desc = [
         ByteField("descryptor_type", 1), # 1 octet
         ShortField("key_information", 4), # 1 octet
    ]

    def extract_padding(self, s):
        return "", s
```

Puis lier ces deux classes via la fonction bind, toujours dans le même fichier mais à la fin.

```python3
bind_layers(EAPOL, EAPOL_KEY)
```


ou la version suivante pour n'être déclancher qu'en cas de EAPoL.type "EAPoL-Key" :

```python3
bind_layers(EAPOL, EAPOL_KEY, type=0x03)
```

On peut ensuite constater que les champs sont bien interprétée.

```bash
####[ EAPOL ]###
                 version   = 802.1X-2001
                 type      = EAPOL-Key
                 len       = 123
####[ EAPoL-Key ]###
                 descryptor_type= 2
####[ Padding ]###
                 load      = '\x01\n\x00\x00\x00\x00\x00\x00 ... '
```

#### Ajouter la "Layer" BSS transition management


Même procédure que précédemment.

```python3
from scapy.layers.dot11 import *

class WIRELESS_MANAGMENT(Dot11FCS):
    name = "WIRELESS_MANAGMENT"

    fields_desc = [
         ByteField("categorie_code", 1), # 1 octet
    ]

    def extract_padding(self, s):
        return "", s

bind_layers(DOT11FCS, WIRELESS_MANAGMENT)
```

#### Action "Layers" WIRELESS_MANAGMENT

Même procédure que précédemment.

```python3
class WIRELESS_MANAGMENT_FIXED_PARAMETERS(Dot11FCS):
    name = "WIRELESS_MANAGMENT"

    fields_desc = [
         ByteField("categorie_code", 1), # 1 octet
         ByteField("action_code", 1), # 1 octet
         ByteField("dialog_token", 1), # 1 octet
         ConditionalField(ByteField("bss_transition_status_code", 1), lambda pkt: pkt.action_code == 8),
         ConditionalField(ByteField("termination_delay", 1), lambda pkt: pkt.action_code == 8),
         ConditionalField(ByteField("unknown_flag", 1), lambda pkt: pkt.action_code == 7),
         ConditionalField(ShortField("disassociation_timer", 1), lambda pkt: pkt.action_code == 7),
         ConditionalField(ByteField("validty_interval", 1), lambda pkt: pkt.action_code == 7),
    ]

    def extract_padding(self, s):
        return "", s

bind_layers(Dot11FCS, WIRELESS_MANAGMENT_FIXED_PARAMETERS)
```

