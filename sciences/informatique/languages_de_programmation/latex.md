# Latex

## Définition

## Documentation 

## Utilisation

Un document latex est composé de deux parties, le préambule et le corp du texte.

Le préambule comprend la classification du document ainsi que l'appel des "packages".
Le corp du document comprend quant à lui le texte et les figures du documents.

#### Préambule

#### Classification Document

```latex
\documentclass[<option>]{<type de document>}
```

| Option | Description         |
|--------|---------------------|
| 11pt   | taille de police 11 |

| Type de document | Description |
|------------------|-------------|
| Article          |             |
| beamer           |             |


#### Import des Package

#### Ajuter la marge

```latex
\usepackage[margin=1.25in]{geometry}
```

### Corp du texte

#### Définir le corp du texte

```latex
\begin{document}
...
\end{document}
```

#### Modifier les options de texte

Voici les options de texte courants.

```latex
{\LARGE large}
\textbf{gras}
\textit{italic}
\underline{texte Large}
```

Ces options peuvent être additionnée.

```latex
\textbf{\LARGE large}
```

Latex rencontre des difficulté à souligner du texte. Il est plus pratique d'utiliser
le pakckage uuline.

```latex
\usepackage{uline}
...
\uline{souligne}
```
