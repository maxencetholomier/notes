# HTML 

## Présentation

Le "Hyper Text Markup Language" est un language de programmation descriptif 

# Documentation

# Utilisation

## Ancres

```html
```

## Lien 

```html
<a href="https://openclassrooms.com">
<a href="page2.html">
<a href="#anchor">
<a href="page2.html#anchor">
<a href="mailto:votrenom@bidule.com">Envoyez-moi un e-mail !</a>

# Lien avec titre lors d'un survolement avec la souris ainsi 
# qu'ouverture vers une nouvelle page au click

<a href="https://openclassrooms.com" title="Vous ne le regretterez pas !" target="_blank">OpenClassrooms</a>
```

