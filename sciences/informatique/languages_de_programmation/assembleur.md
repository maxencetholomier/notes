# Assembleur 

## Définition 

L'assembleur est un langage de programmation bas-niveau. 

Il permet de programmer le CPU en intéragissant avec ses registres de travail 
(r0, ..., r14 sur arm). 

Il n'est aujourd'hui pas nécessaire de savoir programmer en assembleur.
Cependant une bonne connaissance de ce langage est très utile pour le debugge 
en développement embarquées.   

## Documentation 

## Architecture 

Il existe deux grand type d'architecture, à savoir 

| Type                               | Description                                   |
|------------------------------------|-----------------------------------------------|
| Risk (Reduced Instruction ...)     | Beaucoup de registres et peu d'instructions   |
| Cisc (Complex Instruction Set ...) | Peu de registres mais beaucoup d'instructions |

Il existe autant de registre de travail que de langage d'assembleurs.
En embarquée l'on ne s'intéresse qu'aux architectures basse consommation 
tel que : MIPS, ARM, Powerpc, x86.  

## Element du language   

(To Do : remplir cette partie)

### Etiquette 
### Label  
### Registre de travail (ARM)  

