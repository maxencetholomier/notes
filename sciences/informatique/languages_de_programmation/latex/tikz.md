# Tikz

## Définition

## Documentation 

 - [Documentation]{https://tikz.dev/}
 - [Tutoriel]{https://tikz.dev/tutorials-guidelines}

## Utilisation

<!-- TODO : Récupérer les exemple de tests tex dans code exemple. -->

soit :

```latex
\begin{tikzpicture}
\draw (0,0)--(1,2);
\end{tikzpicture}
```

soit :

```latex
\tikz \draw (0,0)--(1,2);
```

