# C++

# Définition

Le langage C++ est un language de programmation compilé créée en 1983 par
Bjarne Stroustrup.
Son but est d'améliorer le langage C en y implémentant un certain nombre de
fonctionnalités (POO, surcharge des fonctions, ...).

Au vu de sa proximité avec le C et son paradigme POO, le C++ est très utilisé
pour le développement de nombreuse application bas-niveau.

# Documentation
